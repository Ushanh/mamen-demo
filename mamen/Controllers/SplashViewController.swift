//
//  SplashViewController.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 6/14/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // let splashView = SplashView(frame: self.view.frame)
        // self.view.addSubview(splashView)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "IntroViewController")
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
