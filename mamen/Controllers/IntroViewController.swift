//
//  IntroViewController.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 6/14/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import UIKit

public enum SCPageStyle: Int {
    case SCNormal = 100
    case SCJAMoveCircle // Design by Jardson Almeida
    case SCJAFillCircle // Design by Jardson Almeida
    case SCJAFlatBar // Design by Jardson Almeida
}

class IntroViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet var bottomViewHeight: NSLayoutConstraint!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var scrollContainerView: UIView!
    @IBOutlet var bottomViewBottomConstraint: NSLayoutConstraint!
    
    // particle x axis
    @IBOutlet var greenParticleX: NSLayoutConstraint!
    @IBOutlet var redParticleX: NSLayoutConstraint!
    @IBOutlet var purpleParticleX: NSLayoutConstraint!
    
    //particles
    @IBOutlet var greenParticle: UIImageView!
    @IBOutlet var redParticle: UIImageView!
    @IBOutlet var purpleParticle: UIImageView!
    
    // page indicator views
    @IBOutlet var firstPageIndicator: PageIndicatorView!
    @IBOutlet var secondPageIndicator: PageIndicatorView!
    @IBOutlet var thirdPageIndicator: PageIndicatorView!
    @IBOutlet var forthPageIndicator: PageIndicatorView!
    
    @IBOutlet var indicatorContainer: UIView!
    
    // page indicator widths
    @IBOutlet var firstIndicatorWidth: NSLayoutConstraint!
    @IBOutlet var secondIndicatorWidth: NSLayoutConstraint!
    @IBOutlet var thirdIndicatorWidth: NSLayoutConstraint!
    @IBOutlet var forthIndicatorWidth: NSLayoutConstraint!
    
    @IBOutlet var firstViewSplash: UIView!
    
    @IBOutlet var viewContainerView: UIView!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet weak var purpleBottomConstraint: NSLayoutConstraint!
    
    var introViewDidAppear = false
    
    //MARK: Properties
    var currentPage: Int = 0 {
        willSet {
            if currentPage != newValue {
                animateExit(index: currentPage)
            }
        }
        didSet {
            if oldValue != currentPage {
                animateEntrance(index: currentPage)
            }
        }
    }
    
    // intro views
    var splashView: UIView!
    var firstIntroView: FirstIntroMainView!
    var secondIntroView: SecondIntroMainView!
    var thirdIntroView: ThirdIntroMainView!
    var forthIntroView: UIView!
    var fifthIntroView: UIView!
    var loginSelectionView: LoginSelectionView!
    var skipButtonPressed = false
    
    var numberOfIntroViews = 5
    
    var scrollPosition: CGFloat = 0.0 {
        didSet {
            if scrollPosition < 0 && scrollPosition > 1 {
                
            } else if scrollPosition < 1 && scrollPosition > 2 {
                
            } else if scrollPosition < 2 && scrollPosition > 3 {
                
            } else if scrollPosition < 3 && scrollPosition > 4 {
                
            }
        }
    }
    
    // splash view
    @IBOutlet var splashImageView: UIImageView!
    @IBOutlet var poweredByLogo: UIImageView!
    @IBOutlet var mainViewXConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var images = [UIImage]()
        for index in 1...91 {
            let imageName = "logo-animation\(index)"
            let image = UIImage(named: imageName)
            images.append(image!)
        }
        self.splashImageView.animationImages = images
        self.splashImageView.animationDuration = 2
        self.splashImageView.animationRepeatCount = 1
        
        // add pan gestures
        let leftSwipeGesture = UISwipeGestureRecognizer()
        leftSwipeGesture.direction = .left
        leftSwipeGesture.addTarget(self, action: #selector(IntroViewController.didSwipeLeft(sender:)))
        
        let rightSwipeGesture = UISwipeGestureRecognizer()
        rightSwipeGesture.direction = .right
        rightSwipeGesture.addTarget(self, action: #selector(IntroViewController.didSwipeRight(sender:)))
        
        self.scrollView.addGestureRecognizer(leftSwipeGesture)
        self.scrollView.addGestureRecognizer(rightSwipeGesture)
        
        if !UIDevice.current.extendedAnimations() {
            self.bottomViewBottomConstraint.constant = -150
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if self.loginSelectionView != nil {
            self.loginSelectionView.splashImageTopConstraint.constant = (self.view.frame.size.height / 2) - (self.view.frame.size.height / 6)
            self.loginSelectionView.splashImageWidthConstraint.constant = 0
            UIView.animate(withDuration: 1.5) {
                self.loginSelectionView.topParticle.alpha = 1
                self.loginSelectionView.bottomParticle.alpha = 1
                self.loginSelectionView.loginSelectionButtonView.alpha = 1
                self.loginSelectionView.layoutIfNeeded()
            }
        }
        
        
        if introViewDidAppear == false {
            
            self.greenParticleX.constant = UIScreen.main.bounds.width
            self.purpleParticleX.constant = UIScreen.main.bounds.width + 200
            view.layoutIfNeeded()
            
            introViewDidAppear = true
            
            setupScrollView()
            loadIntroViews()
            
            self.loginSelectionView.splashImageTopConstraint.constant = (self.view.frame.size.height / 2) - (self.view.frame.size.height / 10)
            self.loginSelectionView.splashImageWidthConstraint.constant = 0
            UIView.animate(withDuration: 1.5) {
                self.loginSelectionView.layoutIfNeeded()
            }
            
            // green particle
            DispatchQueue.main.async {
                let newGreenX = 0 - ((self.greenParticle.frame.size.width/5)*4)
                self.greenParticleX.constant = newGreenX
                self.greenParticle.alpha = 1
                UIView.animate(withDuration: 3, animations: {
                    self.view.layoutIfNeeded()
                }, completion: { (bool) in
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                        
                        let newGreenX: CGFloat = -1000.00
                        self.greenParticleX.constant = newGreenX
                        self.greenParticle.alpha = 1
                        UIView.animate(withDuration: 1.5, animations: {
                            self.view.layoutIfNeeded()
                        }, completion: { (bool) in
                            
                        })
                    })
                })
            }
            
            
            // red particle
            DispatchQueue.main.async {
                
                let newRedX: CGFloat = (self.view.frame.size.width/10)
                //self.view.frame.size.width - ((self.redParticle.frame.size.width/5)*3.5)
                self.redParticleX.constant = newRedX
                self.redParticle.alpha = 1
                UIView.animate(withDuration: 3, animations: {
                    self.view.layoutIfNeeded()
                }, completion: { (bool) in
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                    
                    let newRedX: CGFloat = -1000.00
                    self.redParticleX.constant = newRedX
                    UIView.animate(withDuration: 1.8, animations: {
                        self.view.layoutIfNeeded()
                    }, completion: { (bool) in
                        
                    })
                        
                    })
                })
                
            }
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                
                let newPurpleX: CGFloat = self.view.frame.size.width - (self.purpleParticle.frame.size.width / 4)
                //self.view.frame.size.width - ((self.redParticle.frame.size.width/5)*3.5)
                self.purpleParticleX.constant = newPurpleX
                self.purpleParticle.alpha = 1
                UIView.animate(withDuration: 3, animations: {
                    self.view.layoutIfNeeded()
                }, completion: { (bool) in
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                    
                    let newPurpleX: CGFloat = -1000.00
                    self.purpleParticleX.constant = newPurpleX
                    UIView.animate(withDuration: 1.8, animations: {
                        //self.poweredByLogo.alpha = 0
                        self.view.layoutIfNeeded()
                    }, completion: { (bool) in
                        
                    })
                    UIView.animate(withDuration: 0.5, animations: {
                        self.poweredByLogo.alpha = 0
                    }, completion: { (bool) in
                        
                    })
                        
                    })
                })
            })
            
            // logo image set
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.2, execute: {
                self.splashImageView.image = UIImage(named: "logo-animation91")
                self.splashImageView.startAnimating()
            })
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3, execute: {
                
                UIView.animate(withDuration: 0.6,  animations: {
                    self.poweredByLogo.alpha = 1
                }, completion: { (bool) in
                    
                    UIView.animate(withDuration: 1, delay: 2.2, animations: {
                        self.viewContainerView.frame.origin.x -= self.view.frame.size.width
                    }, completion: { (bool) in
                        self.currentPage = 1
                    })
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2, execute: {
                        self.bottomViewBottomConstraint.constant = 0
                        UIView.animate(withDuration: 2, delay: 0, animations: {
                            self.view.layoutIfNeeded()
                            
                            self.bottomView.alpha = 1
                        }, completion: { (bool) in
                            
                        })
                    })
                    
                    
                    
                })
                
            })
        }
    }
    
    @objc func didSwipeLeft(sender: UISwipeGestureRecognizer) {
        print("did swipe left")
        self.viewNextView()
    }
    
    @objc func didSwipeRight(sender: UISwipeGestureRecognizer) {
        print("did swipe right")
        self.viewPreviousView()
    }
    
    func viewPreviousView() {
        
        if currentPage == 1 || currentPage == 5 {
            return
        }
        
        currentPage-=1
        
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(currentPage - 1)
        frame.origin.y = 0;
        DispatchQueue.main.async {
            self.scrollView.scrollRectToVisible(frame, animated: true)
        }
    }
    
    func viewNextView() {
        
        if currentPage == 5 {
            return
        }
        
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(currentPage)
        frame.origin.y = 0;
        DispatchQueue.main.async {
            self.scrollView.scrollRectToVisible(frame, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Setup Scroll View
    func setupScrollView() {
        
        let scrollViewWidth = self.scrollView.frame.size.width * CGFloat(numberOfIntroViews)
        let contentSize = CGSize(width: scrollViewWidth, height: scrollView.frame.size.height - 68)
        scrollView.contentSize = contentSize
        bottomView.alpha = 0
    }
    
    
    //MARK: Load Intro Views
    func loadIntroViews() {
        
        let height = self.scrollView.frame.size.height - 68
        let width = self.scrollView.frame.size.width
        
        // load first view
        func loadFirstView() {
            self.firstIntroView = Bundle.main.loadNibNamed("FirstIntroView", owner: nil, options: [:])?.first as! FirstIntroMainView
            self.firstIntroView.frame = CGRect(x: 0, y: 0, width: width, height: height)
            self.scrollView.addSubview(firstIntroView)
            firstIntroView.backgroundImage.layer.opacity = 0
        }
        
        // load second view
        func loadSecondView() {
            self.secondIntroView = Bundle.main.loadNibNamed("SecondIntroView", owner: nil, options: [:])?.first as! SecondIntroMainView
            self.secondIntroView.frame = CGRect(x: width, y: 0, width: width, height: height)
            self.scrollView.addSubview(secondIntroView)
            //firstIntroView.backgroundImage.layer.opacity = 0
        }
        
        // load third view
        func loadThirdView() {
            self.thirdIntroView = Bundle.main.loadNibNamed("ThirdIntroView", owner: nil, options: [:])?.first as! ThirdIntroMainView
            self.thirdIntroView.frame = CGRect(x: width*2, y: 0, width: width, height: height)
            self.scrollView.addSubview(thirdIntroView)
            //firstIntroView.backgroundImage.layer.opacity = 0
        }
        
        // load forth view
        func loadForthView() {
            self.forthIntroView = Bundle.main.loadNibNamed("ForthIntroView", owner: nil, options: [:])?.first as! ForthIntroView
            self.forthIntroView.frame = CGRect(x: width*3, y: 0, width: width, height: height)
            self.scrollView.addSubview(forthIntroView)
            //firstIntroView.backgroundImage.layer.opacity = 0
        }
        
        // load login selection view
        func loadLoginSelectionView() {
            self.loginSelectionView = Bundle.main.loadNibNamed("LoginSelectionView", owner: nil, options: [:])?.first as! LoginSelectionView
            self.loginSelectionView.frame = CGRect(x: width*4, y: 0, width: width, height: height+68)
            self.loginSelectionView.delegate = self
            self.scrollView.addSubview(loginSelectionView)
            //firstIntroView.backgroundImage.layer.opacity = 0
        }
        
        loadFirstView()
        loadSecondView()
        loadThirdView()
        loadForthView()
        loadLoginSelectionView()
        
    }
    
    func showSplashParticles() {
        
        
    }
    
    func hideSplashParticles() {
        
        
    }
    
    func animateEntrance(index: Int) {
        switch index {
        case 1:
            UIView.animate(withDuration: 0.5, animations: {
                self.firstIndicatorWidth.constant = 8
                self.firstPageIndicator.alpha = 1
                self.secondPageIndicator.alpha = 0.5
                self.thirdPageIndicator.alpha = 0.5
                self.forthPageIndicator.alpha = 0.5
                self.animateFirstViewEntrance(progress: 0)
            })
            
            break
        case 2:
            UIView.animate(withDuration: 0.5, animations: {
                self.secondIndicatorWidth.constant = 8
                self.firstPageIndicator.alpha = 0.5
                self.secondPageIndicator.alpha = 1
                self.thirdPageIndicator.alpha = 0.5
                self.forthPageIndicator.alpha = 0.5
                self.animateSecondViewEntrance(progress: 0)
            })
            break
        case 3:
            UIView.animate(withDuration: 0.5, animations: {
                self.thirdIndicatorWidth.constant = 8
                self.firstPageIndicator.alpha = 0.5
                self.secondPageIndicator.alpha = 0.5
                self.thirdPageIndicator.alpha = 1
                self.forthPageIndicator.alpha = 0.5
                self.animateThirdViewEntrance(progress: 0)
            })
            break
        case 4:
            UIView.animate(withDuration: 0.5, animations: {
                self.forthIndicatorWidth.constant = 8
                self.firstPageIndicator.alpha = 0.5
                self.secondPageIndicator.alpha = 0.5
                self.thirdPageIndicator.alpha = 0.5
                self.forthPageIndicator.alpha = 1
            })
            break
        case 5:
            UIView.animate(withDuration: 0.5, animations: {
                self.firstPageIndicator.alpha = 0.5
                self.secondPageIndicator.alpha = 0.5
                self.thirdPageIndicator.alpha = 0.5
                self.forthPageIndicator.alpha = 0.5
                //self.loginSelectionView.animateView(progress: 0)
            })
            break
        default:
            break
        }
    }
    
    func animateExit(index: Int) {
        switch index {
        case 1:
            UIView.animate(withDuration: 0.3, animations: {
                self.firstIndicatorWidth.constant = 4
                //self.animateFirstViewExit(progress: 0)
            })
            break
        case 2:
            UIView.animate(withDuration: 0.3, animations: {
                self.secondIndicatorWidth.constant = 4
            })
            break
        case 3:
            UIView.animate(withDuration: 0.3, animations: {
                self.thirdIndicatorWidth.constant = 4
                //self.animateThirdViewExit(progress: 0)
            })
            break
        case 4:
            UIView.animate(withDuration: 0.3, animations: {
                self.forthIndicatorWidth.constant = 4
            })
            break
        case 5:
            UIView.animate(withDuration: 0.3, animations: {
                //self.fifthIndicatorWidth.constant = 4
            })
            break
        case 6:
            UIView.animate(withDuration: 0.3, animations: {
                //self.fifthIndicatorWidth.constant = 4
            })
            break
        default:
            break
        }
    }
    
    //MARK: - Actions
    @IBAction func skipButtonDidPress(_ sender: Any) {
        
        if skipButtonPressed {
            return
        }
        
        skipButtonPressed = true
        
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(numberOfIntroViews-1);
        frame.origin.y = 0;
        DispatchQueue.main.async {
            self.scrollView.scrollRectToVisible(frame, animated: true)
        }
    }
    
    @IBAction func nextButtonDidPress(_ sender: Any) {
        var frame: CGRect = self.scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(currentPage);
        frame.origin.y = 0;
        DispatchQueue.main.async {
            self.scrollView.scrollRectToVisible(frame, animated: true)
        }
    }
    
    @IBAction func loginButtonDidPress(_ sender: Any) {
        
    }
    
    @IBAction func registrationButtonDidPress(_ sender: Any) {
        self.performSegue(withIdentifier: "toRegistrationFLow", sender: self)
    }
    
}

extension IntroViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        //print("scrollViewDidEndScrollingAnimation")
        let pageNo = round(scrollView.contentOffset.x/scrollView.frame.size.width)
        if pageNo >= 0 && Int(round(pageNo)) < numberOfIntroViews {
            self.currentPage = Int(round(pageNo)) + 1
        }
        
        if self.scrollPosition > 0 && self.scrollPosition < 1 {
            
            let remainder = scrollPosition.truncatingRemainder(dividingBy: 1)
            secondIntroView.animateView(progress: remainder)
            
        } else if self.scrollPosition > 1 && self.scrollPosition < 2 {
            
            
            
        } else if self.scrollPosition > 2 && self.scrollPosition < 3 {
            let remainder = scrollPosition.truncatingRemainder(dividingBy: 1)
            self.loginSelectionView.loginSelectionButtonView.alpha = remainder
            
            if scrollView.panGestureRecognizer.translation(in:
                scrollView.superview).x > 0 {
                
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.scrollPosition = scrollView.contentOffset.x/scrollView.frame.size.width
        
        if self.scrollPosition > 3 && self.scrollPosition < 4 {
            self.loginSelectionView.animateView(progress: 0)
            let remainder = scrollPosition.truncatingRemainder(dividingBy: 1)
            //self.loginSelectionView.loginSelectionButtonView.alpha = remainder
            
            if scrollView.panGestureRecognizer.translation(in:
                scrollView.superview).x > 0 {
                
                let position = (0 - (68 * remainder))
                bottomViewBottomConstraint.constant = position
                
            } else {
                
                // set bottom view constraint
                let position = (0 - (68 * remainder))
                bottomViewBottomConstraint.constant =  position
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        self.scrollPosition = scrollView.contentOffset.x/scrollView.frame.size.width
        
        if self.scrollPosition > 3 && self.scrollPosition < 4 {
            self.loginSelectionView.animateView(progress: 0)
            let remainder = scrollPosition.truncatingRemainder(dividingBy: 1)
            //self.loginSelectionView.loginSelectionButtonView.alpha = remainder
            
            if scrollView.panGestureRecognizer.translation(in:
                scrollView.superview).x > 0 {
                
                let position = (0 - (68 * remainder))
                bottomViewBottomConstraint.constant = position
                
            } else {
                
                // set bottom view constraint
                let position = (0 - (68 * remainder))
                bottomViewBottomConstraint.constant =  position
            }
        }
    }
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        print("scrollViewDidEndDecelerating")
//    }
    
//    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
//        scrollViewDidScroll(scrollView)
//    }
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        //print("=========")
//        //scrollViewDidScroll(scrollView)
//    }
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        scrollViewDidScroll(scrollView)
//    }
//
//    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
//        scrollViewDidScroll(scrollView)
//    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//
//    }
}

extension IntroViewController {
    
    // first view
    func animateFirstViewEntrance(progress: CGFloat) {
        firstIntroView.backgroundImage.layer.opacity = 1
        let remainder = scrollPosition.truncatingRemainder(dividingBy: 1)
        firstIntroView.animateView(progress: remainder)
    }
    
    func animateFirstViewExit(progress: CGFloat) {
        firstIntroView.backgroundImage.layer.opacity = 0
    }
    
    // second view
    func animateSecondViewEntrance(progress: CGFloat) {
        //firstIntroView.backgroundImage.layer.opacity = 1
        let remainder = scrollPosition.truncatingRemainder(dividingBy: 1)
        secondIntroView.animateView(progress: progress)
    }
    
    func animateSecondViewExit(progress: CGFloat) {
        
    }
    
    // third view
    func animateThirdViewEntrance(progress: CGFloat) {
        //firstIntroView.backgroundImage.layer.opacity = 1
        let remainder = scrollPosition.truncatingRemainder(dividingBy: 1)
        thirdIntroView.animateView()
    }
    
    func animateThirdViewExit(progress: CGFloat) {
        
    }
}

extension IntroViewController: LoginSelectionProtocol {
    
    func loginDidPress() {
        DispatchQueue.main.async {
            //let newFrame = CGRect(x: self.splashImageView.frame.origin.x, y: 136, width: self.splashImageView.frame.size.width, height: self.splashImageView.frame.size.height)
            self.loginSelectionView.splashImageTopConstraint.constant = 132
            self.loginSelectionView.splashImageWidthConstraint.constant = -42
            UIView.animate(withDuration: 0.7, animations: {
                self.view.layoutIfNeeded()
                self.loginSelectionView.logoImageView.stopAnimating()
                self.loginSelectionView.topParticle.alpha = 0
                self.loginSelectionView.bottomParticle.alpha = 0
                self.loginSelectionView.loginSelectionButtonView.alpha = 0
                //self.splashImageView.frame = newFrame
            }) { (bool) in
                //self.loginSelectionView.logoImageView.image = UIImage(named: "logo")
//                let signUpRootVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginMobileNumberViewController") as! LoginMobileNumberViewController
//                let registrationNavView = UINavigationController(rootViewController: signUpRootVC)
//                registrationNavView.isNavigationBarHidden = true
//                self.present(registrationNavView, animated: false, completion: nil)
            }
        }
    }
    
    func registerDidPress() {
        
    }
}
