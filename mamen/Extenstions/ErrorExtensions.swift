//
//  ErrorExtensions.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 9/21/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import Alamofire

extension Error {
    
    func normalizeError() -> AFValidationError {
        
        let nsError = self as NSError
        if nsError.domain == NSURLErrorDomain {
            if let urlError = (nsError as? URLError) {
                if urlError.code == .notConnectedToInternet || urlError.code == .networkConnectionLost {
                    let validatioError = AFValidationError(id: "internet_error", message: "You need the internet for MAMEN to work. Please check your internet connection and account balance to continue.")
                    return validatioError
                }
            }
        }
        let validatioError = AFValidationError(id: "unknown_error", message: "Oops! Something went wrong. Please try again.")
        return validatioError
    }
}
