//
//  AppDelegate.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 6/14/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import OneSignal
import Fabric
import Crashlytics
import Firebase
//import Answers

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var context: NSManagedObjectContext!
    var pushNotification: [String: Any]? = nil

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        MigrationHelper.shared.configureMigration()
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]

        OneSignal.initWithLaunchOptions(launchOptions, appId: "1594a240-bc61-4acb-9fb5-6047f1555e32", handleNotificationAction: { (openedResult) in
            if let openedResult: [String : Any] = openedResult?.notification.payload.rawPayload as? [String: Any] {
                Answers.logCustomEvent(withName: "Push notification", customAttributes: openedResult)
                self.pushNotification = openedResult
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2, execute: {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HandlePushNotification"), object: self.pushNotification!)
                })
            }
        }, settings: onesignalInitSettings)
        FirebaseApp.configure()
        self.switchNavigation()
        return true
    }
    
    func setPushNotification(userId: Int, subscription: String = "") {
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
        let tagDict = ["user_id": userId]
        OneSignal.sendTags(tagDict)
        if subscription == "on" {
            OneSignal.setSubscription(true)
        } else if subscription == "off" {
            OneSignal.setSubscription(false)
        } else {
            
        }
        if !OneSignal.getPermissionSubscriptionState().permissionStatus.hasPrompted {
            OneSignal.promptForPushNotifications(userResponse: { accepted in
                OneSignal.setSubscription(true)
                //print("User accepted notifications: \(accepted)")
            })
        }
    }
    
    func unSubscribePushNotification() {
        OneSignal.deleteTag("user_id")
        OneSignal.setSubscription(false)
    }
    
    func switchNavigation() {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "IntroNavigationController")
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.post(name: Notification.Name("AppAppearOnForeground"), object: nil)
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "mamen")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if #available(iOS 10.0, *) {
            context = persistentContainer.viewContext
        } else {
            // Fallback on earlier versions
        }
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

//extension AppDelegate: UNUserNotificationCenterDelegate {
//
//    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
//        print(notification)
//    }
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
//        print(userInfo)
//    }
//
//    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [AnyHashable : Any], withResponseInfo responseInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
//        print(userInfo)
//    }
//
//}
