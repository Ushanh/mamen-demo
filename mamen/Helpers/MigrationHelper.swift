//
//  MigrationHelper.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 2/1/18.
//  Copyright © 2018 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
class MigrationHelper {
    
    static let shared = MigrationHelper()
    
    init() {
        
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 3,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 2) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        
    }
    
    func configureMigration() {
        let _ = try! Realm()
    }
}
