//
//  ContactsHelper.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 3/9/18.
//  Copyright © 2018 Ushan Hattotuwa. All rights reserved.
//

import Foundation

class ContactsHelper {
    
    static let shared = ContactsHelper()
    
    private var contacts: [MamenFriend]? = nil
    
    func setContacts(contacts: [MamenFriend]) {
        self.contacts = contacts
    }
    
    func getContacts() -> [MamenFriend]? {
        return self.contacts
    }
}
