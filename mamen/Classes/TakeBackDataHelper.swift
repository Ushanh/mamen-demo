//
//  TakeBackDataHelper.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 8/23/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import UIKit

class TakeBackDataHelper {
    
    static let shared = TakeBackDataHelper()
    
    var userObject: UserObject!
    var unitPrice: UnitPrice!
    var dataObject: UsageData!
    var smsObject: UsageData!
    var voiceObject: UsageData!
    
    var dataTakenBackInMB: Double = 0
    var smsTakenBack: Double = 0
    var voiceTakenBackInSec: Double = 0
    
    var rbTakenBackForData: Double = 0
    var rbTakenBackForSMS: Double = 0
    var rbTakenBackForVoice: Double = 0
    
    func setDataObjects(data: UsageData, sms: UsageData, voice: UsageData) {
        self.dataObject = data
        self.smsObject = sms
        self.voiceObject = voice
    }
    
    func setUserObject(userObject: UserObject) {
        self.userObject = userObject
    }
    
    func setUnitPrices(unitPrice: UnitPrice) {
        self.unitPrice = unitPrice
    }
    
    func getAvailableDataInGB() -> Double {
        if self.dataObject != nil {
            return self.dataObject.available / (1024.0 * 1024.0)
        } else {
            return 0
        }
    }
    
    func getTakeBackDataInMBOrGB() -> (value: Double, isGB: Bool) {
        if self.dataObject != nil {
            let val = Double(self.dataTakenBackInMB) / 1024.0
            if val < 1 {
                return (Double(self.dataTakenBackInMB), false)
            } else {
                return (val, true)
            }
        } else {
            return (0, false)
        }
    }
    
    func getAvailableSMS() -> Double {
        if self.smsObject != nil {
            return self.smsObject.available
        } else {
            return 0
        }
    }
    
    func getAvailableVoice() -> Double {
        if self.voiceObject != nil {
            return self.voiceObject.available
        } else {
            return 0
        }
    }
    
    func getAvailableVoiceInMins() -> Double {
        if self.voiceObject != nil {
            return self.voiceObject.available / 60.0
        } else {
            return 0
        }
    }
    
    func getAvailableDataAfterDeductionInGB() -> Double {
        return (self.dataObject.available / (1024.0 * 1024.0) - (Double(dataTakenBackInMB) / 1024.0))
    }
    
    func getAvailableSMSAfterDeduction() -> Double {
        return self.smsObject.available - Double(smsTakenBack)
    }
    
    func getAvailableVoiceAfterDeductionInMins() -> Double {
        return (self.voiceObject.available - Double(voiceTakenBackInSec)) / 60.0
    }
    
    func submitDataToTakeBack(percentage: Double) -> (deductedInGB: Double, availableInGB: Double) {
        
        let dataToTakeBackInKB = self.dataObject.available * percentage
        let dataToTakeBackInMb = dataToTakeBackInKB / 1024
        let roundedExpectedRBs = (dataToTakeBackInMb * unitPrice.pricePerMB).rounded(.down)
        let roundedDataToTakeBack = roundedExpectedRBs / unitPrice.pricePerMB
        if dataToTakeBackInKB < 0 {
            self.dataTakenBackInMB = 0
        } else {
            self.dataTakenBackInMB = roundedDataToTakeBack
        }
        self.rbTakenBackForData = roundedExpectedRBs
        return (deductedInGB: Double(dataTakenBackInMB) / 1024.0, availableInGB: getAvailableDataAfterDeductionInGB())
    }
    
    func submitVoiceToTakeBack(percentage: Double) -> (deducted: Double, available: Double) {
        
        let voiceToTakeBackSeconds = self.voiceObject.available * percentage
        let voiceToTakeBackMins = voiceToTakeBackSeconds / 60
        let roundedExpectedRBs = (voiceToTakeBackMins * unitPrice.pricePerMin).rounded(.down)
        let roundedVoiceToTakeBack = roundedExpectedRBs / unitPrice.pricePerMin
        if voiceToTakeBackSeconds < 0 {
            self.voiceTakenBackInSec = 0
        } else {
            self.voiceTakenBackInSec = roundedVoiceToTakeBack * 60
        }
        self.rbTakenBackForVoice = roundedExpectedRBs //Double(self.voiceTakenBackInSec / 60) * unitPrice.pricePerMin
        return (deducted: Double(voiceTakenBackInSec / 60), available: getAvailableVoiceAfterDeductionInMins())
    }
    
    func submitTextToTakeBack(percentage: Double) -> (deducted: Double, available: Double) {
        
        let textToTakeBack = self.smsObject.available * percentage
        let roundedExpectedRBs = (textToTakeBack * unitPrice.pricePerSMS).rounded(.down)
        let roundedSMSToTakeBack = (roundedExpectedRBs / unitPrice.pricePerSMS)
        
        if textToTakeBack < 0 {
            self.smsTakenBack = 0
        } else {
            self.smsTakenBack = roundedSMSToTakeBack
        }
        self.rbTakenBackForSMS = roundedExpectedRBs //Double(self.smsTakenBack) * unitPrice.pricePerSMS
        return (deducted: smsTakenBack, available: getAvailableSMSAfterDeduction())
    }
    
    func getRBTakenBackForVoice() -> Double {
        return rbTakenBackForVoice
    }
    
    func getRBTakenBackForData() -> Double {
        return rbTakenBackForData
    }
    
    func getRBTakenBackForSMS() -> Double {
        return rbTakenBackForSMS
    }
    
    func getTakenBackVoiceInSec() -> Double {
        return voiceTakenBackInSec
    }
    
    func getTakenBackVoiceInMin() -> Double {
        return Double(voiceTakenBackInSec) / 60.0
    }
    
    func getTakenBackDataInMB() -> Double {
        return dataTakenBackInMB
    }
    
    func getTakenBackSMS() -> Double {
        return smsTakenBack
    }
    
    func getTotalEarnedRBs() -> Double {
        return getRBTakenBackForVoice() + getRBTakenBackForData()  + getRBTakenBackForSMS()
    }
    
    func getWalletBalanceWithAdditions() -> Double {
        return getTotalEarnedRBs() + Double(userObject.rbBalance!)!
    }
    
    func clearSession() {
        
        dataTakenBackInMB = 0
        smsTakenBack = 0
        voiceTakenBackInSec = 0
        
        rbTakenBackForVoice = 0
        rbTakenBackForSMS = 0
        rbTakenBackForData = 0
    }
    
    func getTakeBackValues() -> (data: Int, voice: Int, sms: Int) {
        return (data: Int(self.getTakenBackDataInMB()), voice: Int(self.getTakenBackVoiceInMin()), sms: Int(self.getTakenBackSMS()))
    }
    
    func shouldShowTakeBackView() -> Bool {
        if getTotalEarnedRBs() < 1 {
            return false
        } else {
            return true
        }
    }
    
    func getFilledDataPercentage() -> CGFloat {
        
        if self.dataObject == nil {
            return 100.00
        }
        
        let remainingPercentage = (Double(self.dataTakenBackInMB * 1024) / self.dataObject.available) * 100
        if remainingPercentage.isNaN {
            return 0.0
        } else {
            return CGFloat(remainingPercentage)
        }
    }
    
    func getFilledSMSPercentage() -> CGFloat {
        
        if self.smsObject == nil {
            return 100.00
        }
        
        let remainingPercentage = (Double(self.smsTakenBack) / self.smsObject.available) * 100
        if remainingPercentage.isNaN {
            return 0.0
        } else {
            return CGFloat(remainingPercentage)
        }
    }
    
    func getFilledVoicePercentage() -> CGFloat {
        
        if self.voiceObject == nil {
            return 100.00
        }
        
        let remainingPercentage = (Double(self.voiceTakenBackInSec) / self.voiceObject.available) * 100
        if remainingPercentage.isNaN {
            return 0.0
        } else {
            return CGFloat(remainingPercentage)
        }
    }
    
    /* changes implementation */
    
    /**
     This function returns the rounded RB balance down to the nearest zero.
     - parameter rule: FloatingPointRoundingRule used to round current RB value
     - returns: Rounded current RB value
     */
    func getRoundedRBBalance(rule: FloatingPointRoundingRule = .down) -> Double {
        return Double(self.userObject.rbBalance ?? "0")!.rounded(rule)
    }
    
    /* changes end */
}
