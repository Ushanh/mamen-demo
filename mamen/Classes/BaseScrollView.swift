//
//  BaseScrollView.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 6/28/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import UIKit

class BaseScrollView: UIScrollView {

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }

}
