//
//  MamenSession.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 8/16/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import RealmSwift

class MamenSession: Object {
    
    @objc dynamic var id = 1
    @objc dynamic var accessToken = ""
    @objc dynamic var tokenValidity = 0
    @objc dynamic var refreshToken = ""
    @objc dynamic var tokenType = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

class MamenSessionHelper {
    
    static func saveSession(session: MamenSession) {
        
        deleteSession()
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(session, update: true)
        }
    }
    
    static func getCurrentSession() -> MamenSession? {
        
        do {
            let realm = try! Realm()
            //let predicate = NSPredicate(format: "id = %d", 1)
            let sessions = realm.objects(MamenSession.self)//.filter(predicate)
            if sessions.count == 0 {
                return nil
            } else {
                return sessions.first
            }
        } catch {
            print(error)
            return nil
        }
    }
    
    static func deleteSession() {
        
        let realm = try! Realm()
        //let predicate = NSPredicate(format: "id = %d", 1)
        let sessions = realm.objects(MamenSession.self)//.filter(predicate)
        if sessions.count > 0 {
            try! realm.write {
                realm.delete(sessions)
            }
        }
        //UserHelper.deleteUserObject()
    }
    
    static func getAuthorizationToken() -> String {
        if let session = getCurrentSession() {
            return "\(session.tokenType ?? "") \(session.accessToken ?? "")"
        } else {
            return ""
        }
    }
}
