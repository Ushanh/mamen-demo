//
//  ChargesText.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 9/15/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import ObjectMapper
import Realm
import RealmSwift

class ChargesText: Object, Mappable {
    
    @objc dynamic var data: String!
    @objc dynamic var sms: String!
    @objc dynamic var voice: String!
    
    required init?(map: Map) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init() {
        super.init()
    }
    
    func mapping(map: Map) {
        data <- map["data"]
        sms <- map["sms"]
        voice <- map["voice"]
    }
    
    
    
}
