//
//  WalletData.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 8/19/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import Realm

class WalletData: Object, Mappable {
    
    @objc dynamic var userObject: UserObject!
    @objc dynamic var data: UsageData?
    @objc dynamic var sms: UsageData?
    @objc dynamic var voice: UsageData?
    @objc dynamic var text: ChargesText?
    @objc dynamic var staticText: String!
    
    required init?(map: Map) {
        super.init()
    }
    
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    func mapping(map: Map) {
        userObject <- map["user"]
        voice <- map["usage.voice"]
        sms <- map["usage.sms"]
        data <- map["usage.data"]
        staticText <- map["usage.static_text"]
        text <- map["usage.text"]
    }
}
