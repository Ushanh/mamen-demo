//
//  UsageData.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 8/17/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import ObjectMapper

class UsageData: Object, Mappable {
    
    @objc dynamic var available: Double = 0
    @objc dynamic var balanceOrder: Double = 0
    @objc dynamic var expiryDateTime = ""
    @objc dynamic var id: Double = 0
    @objc dynamic var max: Double = 0
    @objc dynamic var min: Double = 0
    @objc dynamic var name = ""
    @objc dynamic var reserved: Double = 0
    dynamic var text: List<UsageDataDescription>? = nil
    @objc dynamic var total: Double = 0
    @objc dynamic var unitType: Double = 0
    
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        
        available <- map["available"]
        balanceOrder <- map["balanceOrder"]
        expiryDateTime <- map["expiryDateTime"]
        id <- map["id"]
        max <- map["max"]
        min <- map["min"]
        name <- map["name"]
        reserved <- map["reserved"]
        text <- map["text"]
        total <- map["total"]
        unitType <- map["unitType"]
    }
    
    func getAvailableAmount() -> Double {
        return self.available
    }
    
    func getAvailableDataInGB() -> Double {
        return (self.available / (1024 * 1024))
    }
    
    func getAvailableVoiceInMins() -> Double {
        return self.available / 60.0
    }
    
    func getAvailableSMS() -> Double {
        return self.available
    }
    
    func getUsedPercentage() -> Double {
        let usedPercentage = ((self.total - self.available)/self.total) * 100
        if usedPercentage.isNaN {
            return 0
        } else {
            return usedPercentage.rounded(.toNearestOrAwayFromZero)
        }
    }
    
    func getAvailablePercentageInDecimal() -> Double {
        //let usedAmount = ((self.total - self.available)/self.total) * 100
        let availablePercentage = (self.available / self.total)
        if availablePercentage.isNaN {
            return 0
        } else {
            return availablePercentage
        }
    }
}


class UsageDataDescription: Object, Mappable {
    
    var textString: String!
    
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        
        textString <- map["text"]
    }
}
