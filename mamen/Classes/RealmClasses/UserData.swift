//
//  UserData.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 8/18/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import ObjectMapper

class UserObject: Object, Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var rbBalance: String?
    @objc dynamic var name: String!
    @objc dynamic var avatarUrl: String?
    @objc dynamic var pidUrl: String!
    @objc dynamic var email: String?
    @objc dynamic var username: String!
    @objc dynamic var location: String!
    @objc dynamic var pid: String!
    @objc dynamic var gender: String!
    @objc dynamic var birthday: String!
    @objc dynamic var mobile: String!
    @objc dynamic var createdAt: String?
    @objc dynamic var coreBalance: CoreBalance?
    @objc dynamic var hasNotifications: Bool = false
    @objc dynamic var unreadNotificationsCount: Int = 0
    
    @objc dynamic var avatarData: Data?
    @objc dynamic var pidData: Data?
    
    init(id: Int, name: String, avatar: UIImage?, email: String?, username: String, location: String, pid: String, gender: String, birthday: String, mobile: String) {
        
        super.init()
        
        self.id = id
        self.name = name
        //self.avatar = avatar
        self.email = email
        self.username = NSUserName()
        self.location = location
        self.pid = pid
        self.gender = gender
        self.birthday = birthday
        self.mobile = mobile
    }
    
    required init?(map: Map) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    func getJSONObject() -> NSMutableDictionary {
        let userDict = NSMutableDictionary()
        userDict.setValue(self.name, forKey: "name")
        userDict.setValue(self.username, forKey: "username")
        userDict.setValue(self.username, forKey: "email")
        userDict.setValue(self.location, forKey: "location")
        userDict.setValue(self.pid, forKey: "pid")
        userDict.setValue(self.gender, forKey: "gender")
        userDict.setValue(self.birthday, forKey: "birthday")
        userDict.setValue(self.mobile, forKey: "mobile")
        return userDict
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        avatarUrl <- map["avatar"]
        pidUrl <- map["pid_image"]
        email <- map["email"]
        username <- map["username"]
        location <- map["location"]
        pid <- map["pid"]
        gender <- map["gender"]
        birthday <- map["birthday"]
        mobile <- map["mobile"]
        rbBalance <- map["rb_balance"]
        coreBalance <- map["core_balance"]
        hasNotifications <- map["has_notifications"]
        unreadNotificationsCount <- map["unread_notifications_count"]
    }
    
}


class CoreBalance: Object, Mappable  {
    
    var id: Int!
    dynamic var name: String!
    dynamic var expiryDateTime: String!
    var total: Double!
    var available: Double!
    var reserved: Double!
    var unitType: Double!
    var min: Double!
    var max: Double!
    var balanceOrder: Double!
    
    required init?(map: Map) {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init() {
        super.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        expiryDateTime <- map["expiryDateTime"]
        total <- map["total"]
        available <- map["available"]
        reserved <- map["reserved"]
        unitType <- map["unitType"]
        min <- map["min"]
        max <- map["max"]
        balanceOrder <- map["balanceOrder"]
    }
}

