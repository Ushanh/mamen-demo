//
//  UnitPrice.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 8/19/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import ObjectMapper

struct UnitPrice: Mappable {
    
    var pricePerMB: Double!
    var pricePerSMS: Double!
    var pricePerMin: Double!
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        pricePerMB <- map["mb"]
        pricePerMin <- map["min"]
        pricePerSMS <- map["sms"]
    }
}
