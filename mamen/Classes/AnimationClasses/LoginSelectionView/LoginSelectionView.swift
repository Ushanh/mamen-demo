//
//  LoginSelectionView.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 6/20/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import UIKit

protocol LoginSelectionProtocol {
    func loginDidPress()
    func registerDidPress()
}

class LoginSelectionView: UIView {
    
    //MARK: Properties
    var delegate: LoginSelectionProtocol!
    var imageSet: [UIImage]!
    var animated = false
    
    //@IBOutlet var bottomXConstraint: NSLayoutConstraint!
    //@IBOutlet var topRightXContaint: NSLayoutConstraint!
    
    @IBOutlet var topParticle: UIImageView!
    @IBOutlet var bottomParticle: UIImageView!
    
    //MARK: Outlets
    @IBOutlet var loginSelectionButtonView: UIView!
    @IBOutlet var loginSelectionButtonViewHeight: NSLayoutConstraint!
    @IBOutlet var loginSelectionBottomConstraint: NSLayoutConstraint!
    @IBOutlet var logoImageView: UIImageView!
    @IBOutlet var splashImageTopConstraint: NSLayoutConstraint!
    @IBOutlet var splashImageWidthConstraint: NSLayoutConstraint!
    @IBOutlet var bottomLabelCenterY: NSLayoutConstraint!
    @IBOutlet var yellowParticleBottom: NSLayoutConstraint!
    
    override func draw(_ rect: CGRect) {
        self.imageSet = [UIImage]()
        self.logoImageView.image = UIImage(named: "logo-animation91")
        for index in 1...91 {
            let image = UIImage(named: "logo-animation\(index)")
            self.imageSet.append(image!)
        }
        self.logoImageView.animationImages = self.imageSet
        self.logoImageView.animationDuration = 2
        self.logoImageView.animationRepeatCount = 1
    }
    
    @IBAction func loginButtonDidPress(_ sender: Any) {
        
        if delegate != nil {
            delegate.loginDidPress()
        }
    }
    
    @IBAction func signUpButtonDidPress(_ sender: Any) {
        
        if delegate != nil {
            delegate.registerDidPress()
        }
    }
}

extension LoginSelectionView {
    
    func animateView(progress: CGFloat) {
        
        DispatchQueue.main.async {
            
            self.topParticle.alpha = 0
            self.bottomParticle.alpha = 0
            
            let topImageAnimation = CABasicAnimation(keyPath: "position.x")
            topImageAnimation.fromValue = self.topParticle.layer.position.x + self.frame.size.width + (self.topParticle.layer.position.x * -1)
            topImageAnimation.toValue = self.topParticle.layer.position.x
            topImageAnimation.duration = 3
            self.topParticle.layer.add(topImageAnimation, forKey: topImageAnimation.keyPath)
            self.topParticle.alpha = 1
            
            let bottomImageAnimation = CABasicAnimation(keyPath: "position.x")
            bottomImageAnimation.fromValue = self.frame.size.width
            bottomImageAnimation.toValue = self.bottomParticle.layer.position.x
            bottomImageAnimation.duration = 2
            self.bottomParticle.layer.add(bottomImageAnimation, forKey: bottomImageAnimation.keyPath)
            self.bottomParticle.alpha = 1
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                
                self.bottomLabelCenterY.constant = 0
                self.loginSelectionBottomConstraint.constant = 0
                UIView.animate(withDuration: 1.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: UIView.AnimationOptions.curveEaseIn, animations: {
                    self.layoutIfNeeded()
                }, completion: {
                    (value: Bool) in
                })
            })
        }
        
        
        if logoImageView.isAnimating == false && animated == false {
            self.animated = true
            self.logoImageView.startAnimating()
        }
        
    }
}
