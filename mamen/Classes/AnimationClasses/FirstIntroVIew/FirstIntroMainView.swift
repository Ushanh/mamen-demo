//
//  FirstIntroMainView.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 6/19/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import UIKit

class FirstIntroMainView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    //MARK: Properties
    let opacityAnimation = CABasicAnimation()
    let transformScaleYAnimation = CABasicAnimation()
    
    //MARK: Outlets
    @IBOutlet var backgroundImage: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        
    }
    
    // MARK: - Responder
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        guard let location = touches.first?.location(in: self.superview),
            let hitLayer = self.layer.presentation()?.hitTest(location) else { return }
        
        print("Layer \(hitLayer.name ?? String(describing: hitLayer)) was tapped.")
    }
    
    func startAnimation(percentage: CGRect) {
        
    }
}

extension FirstIntroMainView {
    
    func animateView(progress: CGFloat) {
        
        backgroundImage.layer.name = "Group@3x"
        backgroundImage.layer.contentsGravity = CALayerContentsGravity.center
        backgroundImage.layer.contentsScale = 2
        
        // Group@3x Animations
        //
        
        // transform.rotation.z
        //
        let transformRotationZAnimation = CASpringAnimation()
        transformRotationZAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
        transformRotationZAnimation.duration = 1.4727
        transformRotationZAnimation.speed = 0.5
        transformRotationZAnimation.fillMode = CAMediaTimingFillMode.backwards
        transformRotationZAnimation.isRemovedOnCompletion = false
        transformRotationZAnimation.keyPath = "transform.rotation.z"
        transformRotationZAnimation.toValue = 0
        transformRotationZAnimation.fromValue = 1
        transformRotationZAnimation.stiffness = 100
        transformRotationZAnimation.damping = 10
        transformRotationZAnimation.mass = 1
        transformRotationZAnimation.initialVelocity = 0
        transformRotationZAnimation.isRemovedOnCompletion = true
        backgroundImage.layer.add(transformRotationZAnimation, forKey: "transformRotationZAnimation")
        
        // transform.scale.x
        //
        let transformScaleXAnimation = CABasicAnimation()
        transformScaleXAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
        transformScaleXAnimation.duration = 0.741244
        transformScaleXAnimation.fillMode = CAMediaTimingFillMode.backwards
        transformScaleXAnimation.isRemovedOnCompletion = false
        transformScaleXAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transformScaleXAnimation.keyPath = "transform.scale.x"
        transformScaleXAnimation.toValue = 1
        transformScaleXAnimation.fromValue = 0.5
        transformScaleXAnimation.isRemovedOnCompletion = true
        backgroundImage.layer.add(transformScaleXAnimation, forKey: "transformScaleXAnimation")
        
        // transform.scale.y
        //
        
        transformScaleYAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
        transformScaleYAnimation.duration = 0.741244
        transformScaleYAnimation.fillMode = CAMediaTimingFillMode.backwards
        transformScaleYAnimation.isRemovedOnCompletion = false
        transformScaleYAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transformScaleYAnimation.keyPath = "transform.scale.y"
        transformScaleYAnimation.toValue = 1
        transformScaleYAnimation.fromValue = 0.5
        transformScaleYAnimation.isRemovedOnCompletion = true
        backgroundImage.layer.add(transformScaleYAnimation, forKey: "transformScaleYAnimation")
        
        // opacity
        //
        
        opacityAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
        opacityAnimation.duration = 0.499643
        opacityAnimation.fillMode = CAMediaTimingFillMode.backwards
        opacityAnimation.isRemovedOnCompletion = false
        opacityAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        opacityAnimation.keyPath = "opacity"
        opacityAnimation.toValue = 1
        opacityAnimation.fromValue = 0
        opacityAnimation.isRemovedOnCompletion = true
        backgroundImage.layer.add(opacityAnimation, forKey: "opacityAnimation")
    }

}
