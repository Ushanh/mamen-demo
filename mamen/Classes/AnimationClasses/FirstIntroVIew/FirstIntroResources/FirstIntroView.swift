//
//  Onboard1View.swift
//  Exported from Kite Compositor for Mac 1.4
//
//  Created on 6/19/17, 11:51 AM.
//


import UIKit

class Onboard1View: UIView
{

    // MARK: - Initialization

    init()
    {
        super.init(frame: CGRect(x: 0, y: 0, width: 375, height: 375))
        self.setupLayers()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.setupLayers()
    }

    // MARK: - Setup Layers

    private func setupLayers()
    {
        // Images
        //
        guard let logoPngImage = UIImage(named: "logo1-02.png") else {
            fatalError("Warning: Unable to locate image named 'logo1-02.png'")
        }

        guard let groupXPngImage = UIImage(named: "Group@3x.png") else {
            fatalError("Warning: Unable to locate image named 'Group@3x.png'")
        }

        // onboard1
        //
        let onboardLayer = CALayer()
        onboardLayer.name = "onboard1"
        onboardLayer.bounds = CGRect(x: 0, y: 0, width: 375, height: 375)
        onboardLayer.position = CGPoint(x: 0, y: 0)
        onboardLayer.anchorPoint = CGPoint(x: 0, y: 0)
        onboardLayer.contentsGravity = kCAGravityCenter
        onboardLayer.contentsScale = 2
        onboardLayer.masksToBounds = true

            // onboard1 Sublayers
            //

            // logo1-02
            //
            let logoLayer = CALayer()
            logoLayer.name = "logo1-02"
            logoLayer.bounds = CGRect(x: 0, y: 0, width: 72, height: 85)
            logoLayer.position = CGPoint(x: 152, y: 139)
            logoLayer.anchorPoint = CGPoint(x: 0, y: 0)
            logoLayer.contents = logoPngImage.cgImage
            logoLayer.contentsGravity = kCAGravityCenter
            logoLayer.contentsScale = 2

            onboardLayer.addSublayer(logoLayer)

            // Group@3x
            //
            let groupXLayer = CALayer()
            groupXLayer.name = "Group@3x"
            groupXLayer.bounds = CGRect(x: 0, y: 0, width: 325, height: 323)
            groupXLayer.position = CGPoint(x: 187.5, y: 187.5)
            groupXLayer.contents = groupXPngImage.cgImage
            groupXLayer.contentsGravity = kCAGravityCenter
            groupXLayer.contentsScale = 2

                // Group@3x Animations
                //

                // transform.rotation.z
                //
                let transformRotationZAnimation = CASpringAnimation()
                transformRotationZAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
                transformRotationZAnimation.duration = 1.4727
                transformRotationZAnimation.speed = 0.5
                transformRotationZAnimation.fillMode = kCAFillModeBackwards
                transformRotationZAnimation.isRemovedOnCompletion = false
                transformRotationZAnimation.keyPath = "transform.rotation.z"
                transformRotationZAnimation.toValue = 0
                transformRotationZAnimation.fromValue = 1
                transformRotationZAnimation.stiffness = 100
                transformRotationZAnimation.damping = 10
                transformRotationZAnimation.mass = 1
                transformRotationZAnimation.initialVelocity = 0

                groupXLayer.add(transformRotationZAnimation, forKey: "transformRotationZAnimation")

                // transform.scale.x
                //
                let transformScaleXAnimation = CABasicAnimation()
                transformScaleXAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
                transformScaleXAnimation.duration = 0.741244
                transformScaleXAnimation.fillMode = kCAFillModeBackwards
                transformScaleXAnimation.isRemovedOnCompletion = false
                transformScaleXAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transformScaleXAnimation.keyPath = "transform.scale.x"
                transformScaleXAnimation.toValue = 1
                transformScaleXAnimation.fromValue = 0.5

                groupXLayer.add(transformScaleXAnimation, forKey: "transformScaleXAnimation")

                // transform.scale.y
                //
                let transformScaleYAnimation = CABasicAnimation()
                transformScaleYAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
                transformScaleYAnimation.duration = 0.741244
                transformScaleYAnimation.fillMode = kCAFillModeBackwards
                transformScaleYAnimation.isRemovedOnCompletion = false
                transformScaleYAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transformScaleYAnimation.keyPath = "transform.scale.y"
                transformScaleYAnimation.toValue = 1
                transformScaleYAnimation.fromValue = 0.5

                groupXLayer.add(transformScaleYAnimation, forKey: "transformScaleYAnimation")

                // opacity
                //
                let opacityAnimation = CABasicAnimation()
                opacityAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
                opacityAnimation.duration = 0.499643
                opacityAnimation.fillMode = kCAFillModeBackwards
                opacityAnimation.isRemovedOnCompletion = false
                opacityAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                opacityAnimation.keyPath = "opacity"
                opacityAnimation.toValue = 1
                opacityAnimation.fromValue = 0

                groupXLayer.add(opacityAnimation, forKey: "opacityAnimation")

            onboardLayer.addSublayer(groupXLayer)

        self.layer.addSublayer(onboardLayer)

    }

    // MARK: - Responder

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        guard let location = touches.first?.location(in: self.superview),
              let hitLayer = self.layer.presentation()?.hitTest(location) else { return }

        print("Layer \(hitLayer.name ?? String(describing: hitLayer)) was tapped.")
    }
}
