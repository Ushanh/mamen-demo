//
//  ForthIntroView.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 6/20/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import UIKit

class ForthIntroView: UIView {

    //MARK: Outlets
    @IBOutlet var imageView: UIImageView!
    var imageSet = [UIImage]()
    
    override func draw(_ rect: CGRect) {
        
        self.imageSet = [UIImage]()
        
        if UIDevice.current.extendedAnimations() {
            for index in 1...120 {
                let tempImage = UIImage(named: "raalhu-image\(index)")
                self.imageSet.append(tempImage!)
            }
            self.imageView.animationImages = self.imageSet
            self.imageView.animationDuration = 2
            self.imageView.startAnimating()
        } else {
            self.imageView.alpha = 1
            self.imageView.image = #imageLiteral(resourceName: "raalhu-image80")
        }
        
        
    }
}

extension ForthIntroView {
    
    func animateView(progress: CGFloat) {
        if UIDevice.current.extendedAnimations() {
            self.imageView.startAnimating()
        } else {
            self.imageView.alpha = 1
            self.imageView.image = #imageLiteral(resourceName: "raalhu-image80")
        }
        
    }
}
