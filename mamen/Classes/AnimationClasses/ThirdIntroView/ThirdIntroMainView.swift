//
//  Onboard1View.swift
//  Exported from Kite Compositor for Mac 1.4
//
//  Created on 6/19/17, 12:47 PM.
//


import UIKit

class ThirdIntroMainView: UIView {
    
    // MARK: - Outlets
    @IBOutlet var animationContainerView: UIView!
    @IBOutlet var transferImageView: UIImageView!
    var imageSet = [UIImage]()
    
    // MARL: - Properties
    let circlesXLayer = CALayer()
    let coinXLayer = CALayer()
    let coinXLayer1 = CALayer()
    let coinXLayer2 = CALayer()
    let coinXLayer3 = CALayer()
    let botPhoneXLayer = CALayer()
    let topPhoneXLayer = CALayer()
    
    let transformTranslationYAnimation = CABasicAnimation()
    let transformScaleYAnimation = CABasicAnimation()
    let transformScaleXAnimation = CABasicAnimation()
    let transformRotationZAnimation = CABasicAnimation()
    let transformTranslationYAnimation1 = CABasicAnimation()
    let transformTranslationXAnimation = CABasicAnimation()
    let transformScaleYAnimation1 = CABasicAnimation()
    let transformScaleXAnimation1 = CABasicAnimation()
    let transformRotationZAnimation1 = CABasicAnimation()
    let transformTranslationYAnimation2 = CABasicAnimation()
    let transformTranslationXAnimation1 = CABasicAnimation()
    let transformScaleYAnimation2 = CABasicAnimation()
    let transformScaleXAnimation2 = CABasicAnimation()
    let transformTranslationYAnimation3 = CABasicAnimation()
    let transformScaleYAnimation3 = CABasicAnimation()
    let transformScaleXAnimation3 = CABasicAnimation()
    let transformRotationZAnimation4 = CAKeyframeAnimation()
    let transformRotationZAnimation3 = CABasicAnimation()
    let transformScaleXAnimation4 = CAKeyframeAnimation()
    let transformScaleYAnimation4 = CAKeyframeAnimation()
    let transformRotationZAnimation2 = CABasicAnimation()
    
    // MARK: - Initialization
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
    }
    
    override func draw(_ rect: CGRect) {
        self.imageSet = [UIImage]()
        if UIDevice.current.extendedAnimations() {
            for index in 1...89 {
                let tempImage = UIImage(named: "transfer-image\(index)")
                self.imageSet.append(tempImage!)
            }
            self.transferImageView.animationImages = self.imageSet
            self.transferImageView.startAnimating()
        } else {
            self.transferImageView.alpha = 1
            self.transferImageView.image = #imageLiteral(resourceName: "transfer-image44")
        }
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }

    // MARK: - Responder
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        guard let location = touches.first?.location(in: self.superview),
              let hitLayer = self.layer.presentation()?.hitTest(location) else { return }
        
        print("Layer \(hitLayer.name ?? String(describing: hitLayer)) was tapped.")
    }
}

extension ThirdIntroMainView {
    
    func animateView() {
        
        if UIDevice.current.extendedAnimations() {
            DispatchQueue.main.async {
                //self.transferImageView.animationImages = self.imageSet
                self.transferImageView.startAnimating()
            }
        } else {
            self.transferImageView.alpha = 1
            self.transferImageView.image = #imageLiteral(resourceName: "transfer-image44")
        }
    }
    
        
//    func animateView(progress: CGFloat) {
//        // Images
//        //
//        guard let coinXPngImage = UIImage(named: "coin1@3x.png") else {
//            fatalError("Warning: Unable to locate image named 'coin1@3x.png'")
//        }
//
//        guard let coinXPngImage1 = UIImage(named: "coin2@3x.png") else {
//            fatalError("Warning: Unable to locate image named 'coin2@3x.png'")
//        }
//
//        guard let coinXPngImage2 = UIImage(named: "coin3@3x.png") else {
//            fatalError("Warning: Unable to locate image named 'coin3@3x.png'")
//        }
//
//        guard let coinXPngImage3 = UIImage(named: "coin4@3x.png") else {
//            fatalError("Warning: Unable to locate image named 'coin4@3x.png'")
//        }
//
//        guard let circlesXPngImage = UIImage(named: "circles@3x.png") else {
//            fatalError("Warning: Unable to locate image named 'circles@3x.png'")
//        }
//
//        guard let topPhoneXPngImage = UIImage(named: "top_phone@3x.png") else {
//            fatalError("Warning: Unable to locate image named 'top_phone@3x.png'")
//        }
//
//        guard let botPhoneXPngImage = UIImage(named: "bot_phone@3x.png") else {
//            fatalError("Warning: Unable to locate image named 'bot_phone@3x.png'")
//        }
//
//        circlesXLayer.removeFromSuperlayer()
//
//        // onboard1
//        //
//        let onboardLayer = CALayer()
//        onboardLayer.name = "onboard1"
//        onboardLayer.bounds = CGRect(x: 0, y: 0, width: self.animationContainerView.frame.size.width, height: self.animationContainerView.frame.size.height)
//        onboardLayer.position = CGPoint(x: 0, y: 0)
//        onboardLayer.anchorPoint = CGPoint(x: 0, y: 0)
//        onboardLayer.contentsGravity = kCAGravityCenter
//        onboardLayer.contentsScale = 2
//        onboardLayer.masksToBounds = true
//
//        // onboard1 Sublayers
//        //
//
//        // coin1@3x
//        //
//        //let coinXLayer = CALayer()
//        coinXLayer.name = "coin1@3x"
//        coinXLayer.bounds = CGRect(x: 0, y: 0, width: 40, height: 40)
//        coinXLayer.position = CGPoint(x: 189, y: 105)
//        coinXLayer.contents = coinXPngImage.cgImage
//        coinXLayer.contentsGravity = kCAGravityCenter
//        coinXLayer.contentsScale = 2
//
//        // coin1@3x Animations
//        //
//
//        // transform.translation.y
//        //
//        //let transformTranslationYAnimation = CABasicAnimation()
//        transformTranslationYAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.49
//        transformTranslationYAnimation.duration = 0.999999
//        transformTranslationYAnimation.fillMode = kCAFillModeBackwards
//        transformTranslationYAnimation.isRemovedOnCompletion = false
//        transformTranslationYAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformTranslationYAnimation.keyPath = "transform.translation.y"
//        transformTranslationYAnimation.toValue = 0
//        transformTranslationYAnimation.fromValue = 169
//        transformTranslationYAnimation.repeatCount = HUGE
//        coinXLayer.add(transformTranslationYAnimation, forKey: "transformTranslationYAnimation")
//
//        // transform.scale.y
//        //
//        //let transformScaleYAnimation = CABasicAnimation()
//        transformScaleYAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.49
//        transformScaleYAnimation.duration = 0.999999
//        transformScaleYAnimation.fillMode = kCAFillModeBackwards
//        transformScaleYAnimation.isRemovedOnCompletion = false
//        transformScaleYAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformScaleYAnimation.keyPath = "transform.scale.y"
//        transformScaleYAnimation.toValue = 1
//        transformScaleYAnimation.fromValue = 0.6
//        transformScaleYAnimation.repeatCount = HUGE
//        coinXLayer.add(transformScaleYAnimation, forKey: "transformScaleYAnimation")
//
//        // transform.scale.x
//        //
//        //let transformScaleXAnimation = CABasicAnimation()
//        transformScaleXAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.49
//        transformScaleXAnimation.duration = 0.999999
//        transformScaleXAnimation.fillMode = kCAFillModeBackwards
//        transformScaleXAnimation.isRemovedOnCompletion = false
//        transformScaleXAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformScaleXAnimation.keyPath = "transform.scale.x"
//        transformScaleXAnimation.toValue = 1
//        transformScaleXAnimation.fromValue = 0.6
//        transformScaleXAnimation.repeatCount = HUGE
//        coinXLayer.add(transformScaleXAnimation, forKey: "transformScaleXAnimation")
//
//        // transform.rotation.z
//        //
//        //let transformRotationZAnimation = CABasicAnimation()
//        transformRotationZAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.49
//        transformRotationZAnimation.duration = 0.999999
//        transformRotationZAnimation.fillMode = kCAFillModeBackwards
//        transformRotationZAnimation.isRemovedOnCompletion = false
//        transformRotationZAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformRotationZAnimation.keyPath = "transform.rotation.z"
//        transformRotationZAnimation.toValue = -3
//        transformRotationZAnimation.fromValue = 0
//        transformRotationZAnimation.repeatCount = HUGE
//        coinXLayer.add(transformRotationZAnimation, forKey: "transformRotationZAnimation")
//
//        onboardLayer.addSublayer(coinXLayer)
//
//        // coin2@3x
//        //
//        //let coinXLayer1 = CALayer()
//        coinXLayer1.name = "coin2@3x"
//        coinXLayer1.bounds = CGRect(x: 0, y: 0, width: 40, height: 40)
//        coinXLayer1.position = CGPoint(x: 189, y: 105)
//        coinXLayer1.contents = coinXPngImage1.cgImage
//        coinXLayer1.contentsGravity = kCAGravityCenter
//        coinXLayer1.contentsScale = 2
//
//        // coin2@3x Animations
//        //
//
//        // transform.translation.y
//        //
//        //let transformTranslationYAnimation1 = CABasicAnimation()
//        transformTranslationYAnimation1.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.336
//        transformTranslationYAnimation1.duration = 0.999999
//        transformTranslationYAnimation1.fillMode = kCAFillModeBackwards
//        transformTranslationYAnimation1.isRemovedOnCompletion = false
//        transformTranslationYAnimation1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformTranslationYAnimation1.keyPath = "transform.translation.y"
//        transformTranslationYAnimation1.toValue = 0
//        transformTranslationYAnimation1.fromValue = 169
//        transformTranslationYAnimation1.repeatCount = HUGE
//        coinXLayer1.add(transformTranslationYAnimation1, forKey: "transformTranslationYAnimation1")
//
//        // transform.translation.x
//        //
//        //let transformTranslationXAnimation = CABasicAnimation()
//        transformTranslationXAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.336
//        transformTranslationXAnimation.duration = 0.999999
//        transformTranslationXAnimation.fillMode = kCAFillModeBackwards
//        transformTranslationXAnimation.isRemovedOnCompletion = false
//        transformTranslationXAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformTranslationXAnimation.keyPath = "transform.translation.x"
//        transformTranslationXAnimation.toValue = 12
//        transformTranslationXAnimation.fromValue = 0
//        transformTranslationYAnimation.repeatCount = HUGE
//        coinXLayer1.add(transformTranslationXAnimation, forKey: "transformTranslationXAnimation")
//
//        // transform.scale.y
//        //
//        //let transformScaleYAnimation1 = CABasicAnimation()
//        transformScaleYAnimation1.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.336
//        transformScaleYAnimation1.duration = 0.999999
//        transformScaleYAnimation1.fillMode = kCAFillModeBackwards
//        transformScaleYAnimation1.isRemovedOnCompletion = false
//        transformScaleYAnimation1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformScaleYAnimation1.keyPath = "transform.scale.y"
//        transformScaleYAnimation1.toValue = 1
//        transformScaleYAnimation1.fromValue = 0.6
//        transformScaleYAnimation1.repeatCount = HUGE
//        coinXLayer1.add(transformScaleYAnimation1, forKey: "transformScaleYAnimation1")
//
//        // transform.scale.x
//        //
//        //let transformScaleXAnimation1 = CABasicAnimation()
//        transformScaleXAnimation1.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.336
//        transformScaleXAnimation1.duration = 0.999999
//        transformScaleXAnimation1.fillMode = kCAFillModeBackwards
//        transformScaleXAnimation1.isRemovedOnCompletion = false
//        transformScaleXAnimation1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformScaleXAnimation1.keyPath = "transform.scale.x"
//        transformScaleXAnimation1.toValue = 1
//        transformScaleXAnimation1.fromValue = 0.6
//        transformScaleXAnimation1.repeatCount = HUGE
//        coinXLayer1.add(transformScaleXAnimation1, forKey: "transformScaleXAnimation1")
//
//        // transform.rotation.z
//        //
//        //let transformRotationZAnimation1 = CABasicAnimation()
//        transformRotationZAnimation1.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.336
//        transformRotationZAnimation1.duration = 0.999999
//        transformRotationZAnimation1.fillMode = kCAFillModeBackwards
//        transformRotationZAnimation1.isRemovedOnCompletion = false
//        transformRotationZAnimation1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformRotationZAnimation1.keyPath = "transform.rotation.z"
//        transformRotationZAnimation1.toValue = -4
//        transformRotationZAnimation1.fromValue = 0
//        transformRotationZAnimation1.repeatCount = HUGE
//        coinXLayer1.add(transformRotationZAnimation1, forKey: "transformRotationZAnimation1")
//
//        onboardLayer.addSublayer(coinXLayer1)
//
//        // coin3@3x
//        //
//        //let coinXLayer2 = CALayer()
//        coinXLayer2.name = "coin3@3x"
//        coinXLayer2.bounds = CGRect(x: 0, y: 0, width: 40, height: 40)
//        coinXLayer2.position = CGPoint(x: 189, y: 105)
//        coinXLayer2.contents = coinXPngImage2.cgImage
//        coinXLayer2.contentsGravity = kCAGravityCenter
//        coinXLayer2.contentsScale = 2
//
//        // coin3@3x Animations
//        //
//
//        // transform.translation.y
//        //
//        //let transformTranslationYAnimation2 = CABasicAnimation()
//        transformTranslationYAnimation2.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.162
//        transformTranslationYAnimation2.duration = 0.999999
//        transformTranslationYAnimation2.fillMode = kCAFillModeBackwards
//        transformTranslationYAnimation2.isRemovedOnCompletion = false
//        transformTranslationYAnimation2.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformTranslationYAnimation2.keyPath = "transform.translation.y"
//        transformTranslationYAnimation2.toValue = 0
//        transformTranslationYAnimation2.fromValue = 169
//        transformTranslationYAnimation2.repeatCount = HUGE
//        coinXLayer2.add(transformTranslationYAnimation2, forKey: "transformTranslationYAnimation2")
//
//        // transform.translation.x
//        //
//        //let transformTranslationXAnimation1 = CABasicAnimation()
//        transformTranslationXAnimation1.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.162
//        transformTranslationXAnimation1.duration = 0.999999
//        transformTranslationXAnimation1.fillMode = kCAFillModeBackwards
//        transformTranslationXAnimation1.isRemovedOnCompletion = false
//        transformTranslationXAnimation1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformTranslationXAnimation1.keyPath = "transform.translation.x"
//        transformTranslationXAnimation1.toValue = -23
//        transformTranslationXAnimation1.fromValue = 0
//        transformTranslationXAnimation1.repeatCount = HUGE
//        coinXLayer2.add(transformTranslationXAnimation1, forKey: "transformTranslationXAnimation1")
//
//        // transform.scale.y
//        //
//        //let transformScaleYAnimation2 = CABasicAnimation()
//        transformScaleYAnimation2.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.162
//        transformScaleYAnimation2.duration = 0.999999
//        transformScaleYAnimation2.fillMode = kCAFillModeBackwards
//        transformScaleYAnimation2.isRemovedOnCompletion = false
//        transformScaleYAnimation2.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformScaleYAnimation2.keyPath = "transform.scale.y"
//        transformScaleYAnimation2.toValue = 1
//        transformScaleYAnimation2.fromValue = 0.6
//        transformScaleYAnimation2.repeatCount = HUGE
//        coinXLayer2.add(transformScaleYAnimation2, forKey: "transformScaleYAnimation2")
//
//        // transform.scale.x
//        //
//        //let transformScaleXAnimation2 = CABasicAnimation()
//        transformScaleXAnimation2.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.162
//        transformScaleXAnimation2.duration = 0.999999
//        transformScaleXAnimation2.fillMode = kCAFillModeBackwards
//        transformScaleXAnimation2.isRemovedOnCompletion = false
//        transformScaleXAnimation2.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformScaleXAnimation2.keyPath = "transform.scale.x"
//        transformScaleXAnimation2.toValue = 1
//        transformScaleXAnimation2.fromValue = 0.6
//        transformScaleXAnimation2.repeatCount = HUGE
//        coinXLayer2.add(transformScaleXAnimation2, forKey: "transformScaleXAnimation2")
//
//        // transform.rotation.z
//        //
//        //let transformRotationZAnimation2 = CABasicAnimation()
//        transformRotationZAnimation2.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.162
//        transformRotationZAnimation2.duration = 0.999999
//        transformRotationZAnimation2.fillMode = kCAFillModeBackwards
//        transformRotationZAnimation2.isRemovedOnCompletion = false
//        transformRotationZAnimation2.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformRotationZAnimation2.keyPath = "transform.rotation.z"
//        transformRotationZAnimation2.toValue = -3
//        transformRotationZAnimation2.fromValue = 0
//        transformRotationZAnimation2.repeatCount = HUGE
//        coinXLayer2.add(transformRotationZAnimation2, forKey: "transformRotationZAnimation2")
//
//        onboardLayer.addSublayer(coinXLayer2)
//
//        // coin4@3x
//        //
//        //let coinXLayer3 = CALayer()
//        coinXLayer3.name = "coin4@3x"
//        coinXLayer3.bounds = CGRect(x: 0, y: 0, width: 40, height: 40)
//        coinXLayer3.position = CGPoint(x: 189, y: 105)
//        coinXLayer3.contents = coinXPngImage3.cgImage
//        coinXLayer3.contentsGravity = kCAGravityCenter
//        coinXLayer3.contentsScale = 2
//
//        // coin4@3x Animations
//        //
//
//        // transform.translation.y
//        //
//        //let transformTranslationYAnimation3 = CABasicAnimation()
//        transformTranslationYAnimation3.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
//        transformTranslationYAnimation3.duration = 0.999999
//        transformTranslationYAnimation3.fillMode = kCAFillModeBackwards
//        transformTranslationYAnimation3.isRemovedOnCompletion = false
//        transformTranslationYAnimation3.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformTranslationYAnimation3.keyPath = "transform.translation.y"
//        transformTranslationYAnimation3.toValue = 0
//        transformTranslationYAnimation3.fromValue = 169
//        transformTranslationYAnimation3.repeatCount = HUGE
//        coinXLayer3.add(transformTranslationYAnimation3, forKey: "transformTranslationYAnimation3")
//
//        // transform.scale.y
//        //
//        //let transformScaleYAnimation3 = CABasicAnimation()
//        transformScaleYAnimation3.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
//        transformScaleYAnimation3.duration = 0.999999
//        transformScaleYAnimation3.fillMode = kCAFillModeBackwards
//        transformScaleYAnimation3.isRemovedOnCompletion = false
//        transformScaleYAnimation3.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformScaleYAnimation3.keyPath = "transform.scale.y"
//        transformScaleYAnimation3.toValue = 1
//        transformScaleYAnimation3.fromValue = 0.6
//        transformScaleYAnimation3.repeatCount = HUGE
//        coinXLayer3.add(transformScaleYAnimation3, forKey: "transformScaleYAnimation3")
//
//        // transform.scale.x
//        //
//        //let transformScaleXAnimation3 = CABasicAnimation()
//        transformScaleXAnimation3.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
//        transformScaleXAnimation3.duration = 0.999999
//        transformScaleXAnimation3.fillMode = kCAFillModeBackwards
//        transformScaleXAnimation3.isRemovedOnCompletion = false
//        transformScaleXAnimation3.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformScaleXAnimation3.keyPath = "transform.scale.x"
//        transformScaleXAnimation3.toValue = 1
//        transformScaleXAnimation3.fromValue = 0.6
//        transformScaleXAnimation3.repeatCount = HUGE
//        coinXLayer3.add(transformScaleXAnimation3, forKey: "transformScaleXAnimation3")
//
//        // transform.rotation.z
//        //
//        //let transformRotationZAnimation3 = CABasicAnimation()
//        transformRotationZAnimation3.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
//        transformRotationZAnimation3.duration = 0.999999
//        transformRotationZAnimation3.fillMode = kCAFillModeBackwards
//        transformRotationZAnimation3.isRemovedOnCompletion = false
//        transformRotationZAnimation3.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transformRotationZAnimation3.keyPath = "transform.rotation.z"
//        transformRotationZAnimation3.toValue = 3
//        transformRotationZAnimation3.fromValue = 0
//        transformRotationZAnimation3.repeatCount = HUGE
//        coinXLayer3.add(transformRotationZAnimation3, forKey: "transformRotationZAnimation3")
//
//        onboardLayer.addSublayer(coinXLayer3)
//
//        // circles@3x
//        //
//        let mainWidth = self.animationContainerView.frame.size.width
//        let mainHeight = self.animationContainerView.frame.size.height
//
//        //var circlesXLayer = CALayer()
//        circlesXLayer.name = "circles@3x"
//        circlesXLayer.bounds = CGRect(x: 0, y: 0, width: mainWidth, height: mainHeight)
//        circlesXLayer.transform = CATransform3DMakeScale(0.7, 0.7, 1)
//        circlesXLayer.position = CGPoint(x: (mainWidth - circlesXLayer.frame.size.width)/2, y: (self.animationContainerView.frame.origin.y + mainHeight/2))
//        circlesXLayer.contents = circlesXPngImage.cgImage
//        circlesXLayer.contentsGravity = kCAGravityCenter
//        circlesXLayer.contentsScale = 2
//
//        // circles@3x Animations
//        //
//
//        // transform.rotation.z
//        //
//        //let transformRotationZAnimation4 = CAKeyframeAnimation()
//        transformRotationZAnimation4.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
//        transformRotationZAnimation4.duration = 1.556998
//        transformRotationZAnimation4.fillMode = kCAFillModeBackwards
//        transformRotationZAnimation4.isRemovedOnCompletion = false
//        transformRotationZAnimation4.keyPath = "transform.rotation.z"
//        transformRotationZAnimation4.values = [ 0, 1.1, 0 ]
//        transformRotationZAnimation4.timingFunctions = [ CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut), CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn) ]
//        transformRotationZAnimation4.calculationMode = kCAAnimationLinear
//        transformRotationZAnimation4.isRemovedOnCompletion = true
//        transformRotationZAnimation4.repeatCount = HUGE
//        circlesXLayer.add(transformRotationZAnimation4, forKey: "transformRotationZAnimation4")
//
//        // transform.scale.x
//        //
//        //let transformScaleXAnimation4 = CAKeyframeAnimation()
//        transformScaleXAnimation4.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
//        transformScaleXAnimation4.duration = 1.556998
//        transformScaleXAnimation4.fillMode = kCAFillModeBackwards
//        transformScaleXAnimation4.isRemovedOnCompletion = false
//        transformScaleXAnimation4.keyPath = "transform.scale.x"
//        transformScaleXAnimation4.values = [ 1, 1.1, 1 ]
//        transformScaleXAnimation4.calculationMode = kCAAnimationLinear
//        transformScaleXAnimation4.isRemovedOnCompletion = true
//        transformScaleXAnimation4.repeatCount = HUGE
//        circlesXLayer.add(transformScaleXAnimation4, forKey: "transformScaleXAnimation4")
//
//        // transform.scale.y
//        //
//        //let transformScaleYAnimation4 = CAKeyframeAnimation()
//        transformScaleYAnimation4.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
//        transformScaleYAnimation4.duration = 1.556998
//        transformScaleYAnimation4.fillMode = kCAFillModeBackwards
//        transformScaleYAnimation4.isRemovedOnCompletion = false
//        transformScaleYAnimation4.keyPath = "transform.scale.y"
//        transformScaleYAnimation4.values = [ 1, 1.1, 1 ]
//        transformScaleYAnimation4.calculationMode = kCAAnimationLinear
//        transformScaleYAnimation4.isRemovedOnCompletion = true
//        transformScaleYAnimation4.repeatCount = HUGE
//        circlesXLayer.add(transformScaleYAnimation4, forKey: "transformScaleYAnimation4")
//
//        onboardLayer.addSublayer(circlesXLayer)
//
//        // top_phone@3x
//        //
//        //let topPhoneXLayer = CALayer()
//        topPhoneXLayer.name = "top_phone@3x"
//        topPhoneXLayer.bounds = CGRect(x: 0, y: 0, width: 98, height: 85)
//        topPhoneXLayer.transform = CATransform3DMakeScale(0.65, 0.65, 1)
//        let topPhonePosition = CGPoint(x: (self.animationContainerView.frame.size.width-98)/2, y: 20)
//        topPhoneXLayer.position = topPhonePosition//CGPoint(x: 140, y: 47)
//        topPhoneXLayer.anchorPoint = CGPoint(x: 0, y: 0)
//        topPhoneXLayer.contents = topPhoneXPngImage.cgImage
//        topPhoneXLayer.contentsGravity = kCAGravityCenter
//        topPhoneXLayer.contentsScale = 2
//
//        onboardLayer.addSublayer(topPhoneXLayer)
//
//        // bot_phone@3x
//        //
//        //let botPhoneXLayer = CALayer()
//        botPhoneXLayer.name = "bot_phone@3x"
//        botPhoneXLayer.bounds = CGRect(x: 0, y: 0, width: 98, height: 93)
//        botPhoneXLayer.transform = CATransform3DMakeScale(0.65, 0.65, 1)
//        botPhoneXLayer.position = CGPoint(x: 140, y: 245)
//        botPhoneXLayer.anchorPoint = CGPoint(x: 0, y: 0)
//        botPhoneXLayer.contents = botPhoneXPngImage.cgImage
//        botPhoneXLayer.contentsGravity = kCAGravityCenter
//        botPhoneXLayer.contentsScale = 2
//
//        onboardLayer.addSublayer(botPhoneXLayer)
//
//        self.animationContainerView.layer.addSublayer(onboardLayer)
//
//    }
}
