//
//  SecondIntroView.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 6/20/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import UIKit

class SecondIntroMainView: UIView {

    @IBOutlet var animationContainerView: UIView!
    @IBOutlet var animationImageView: UIImageView!
    var imageSet: [UIImage]!
    var isAnimating = false
    
    override func draw(_ rect: CGRect) {
        self.imageSet = [UIImage]()
        
        if UIDevice.current.extendedAnimations() {
            for index in 1...80 {
                let str = "walletanim\(index)"
                let tempImage = UIImage(named: str)
                self.imageSet.append(tempImage!)
            }
            self.animationImageView.animationImages = self.imageSet
            self.animationImageView.animationDuration = 2.5
            self.animationImageView.animationRepeatCount = 0
            
        } else {
            self.animationImageView.alpha = 1
            self.animationImageView.image = #imageLiteral(resourceName: "walletanim80")
        }
    }
    
}

extension SecondIntroMainView {
    
    func animateView(progress: CGFloat) {
        if UIDevice.current.extendedAnimations() {
            if isAnimating == false {
                UIView.animate(withDuration: 0.3, animations: {
                    self.animationImageView.alpha = 1
                }, completion: nil)
                self.animationImageView.startAnimating()
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.animationImageView.animationDuration, execute: {
                    self.isAnimating = false
                })
            }
        } else {
            self.animationImageView.alpha = 1
            self.animationImageView.image = #imageLiteral(resourceName: "walletanim80")
        }
    }
}
