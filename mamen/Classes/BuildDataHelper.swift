//
//  BuildDataHelper.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 8/21/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation

class BuildDataHelper {
    
    static let shared = BuildDataHelper()
    
    var userObject: UserObject!
    var unitPrice: UnitPrice!
    var dataObject: UsageData!
    var smsObject: UsageData!
    var voiceObject: UsageData!
    
    var voiceAddedSec: Double = 0
    var smsAdded: Double = 0
    var dataAddedMb: Double = 0
    
    var addedVoiceCharge: Double = 0
    var addedSMSCharge: Double = 0
    var addedDataCharge: Double = 0
    
    var maxSMSCount: Double = 0
    var maxDataInMB: Double = 0
    var maxVoiceInSecs: Double = 0
    
    init() {
        
    }
    
    func setUnitPrices(unitPrice: UnitPrice) {
        self.unitPrice = unitPrice
        
        maxDataInMB = self.getMaxBuildableDataMBs()
        maxSMSCount = self.getMaxBuildableSMS()
        maxVoiceInSecs = self.getMaxBuildableVoiceSecs()
    }
    
    func setDataObjects(data: UsageData, sms: UsageData, voice: UsageData) {
        self.dataObject = data
        self.smsObject = sms
        self.voiceObject = voice
    }
    
    func setUserObject(userObject: UserObject) {
        
        self.userObject = userObject
    }
    
    // int validation
    
//    func validateBuiltDataWithRB(builtDataInMb: Double, allowDouble: Bool = false) -> (totalRBsSpent: Double, totalMBsBuilt: Double) {
//
//        if !allowDouble {
//            let buyableMBsPerRB = (1 / unitPrice.pricePerMB)
//
//        }
//    }
    
    // int validation end
    
//    func getMaxBuildableDataMBs() -> Double {
//
//        let availableAmount = Double(self.userObject.rbBalance ?? "0")! - (self.addedSMSCharge + self.addedVoiceCharge)
//        let buyableMBs = Double(availableAmount / unitPrice.pricePerMB)
//        return buyableMBs
//    }
//
//    func getMaxBuildableSMS() -> Double {
//
//        let availableAmount = Double(self.userObject.rbBalance ?? "0")! - (self.addedDataCharge + self.addedVoiceCharge)
//        let buyableSMSs = Double(availableAmount / unitPrice.pricePerSMS)
//        return buyableSMSs
//    }
//
//    func getMaxBuildableVoiceSecs() -> Double {
//
//        let availableAmount = Double(self.userObject.rbBalance ?? "0")! - (self.addedSMSCharge + self.addedDataCharge)
//        let buyableVoice = Double(availableAmount / unitPrice.pricePerMin)
//        return buyableVoice * 60
//    }
    
    func getRBBalanceRounded(rule: FloatingPointRoundingRule) -> Double {
        return Double(self.userObject.rbBalance ?? "0")!.rounded(rule)
    }
    
    func getCurrentDataCountInGB() -> Double {
        return self.dataObject.available
    }
    
    func getCurrentSMSCount() -> Double {
        return self.smsObject.available
    }
    
    func getCurrentVoiceCountInMins() -> Double {
        return self.voiceObject.available
    }
    
//    func submitDataIncrement(percentage: Double) -> (possible: Bool, addedValue: Double, percentage: Double) {
//
//        let desiredDataValue = maxDataInMB * percentage
//        if desiredDataValue > getMaxBuildableDataMBs() {
//            let perc = (getMaxBuildableDataMBs() / maxDataInMB) * 100
//            if getMaxBuildableDataMBs() < 0 {
//                self.dataAddedMb = 0
//            } else {
//                self.dataAddedMb = getMaxBuildableDataMBs()
//            }
//            self.addedDataCharge = dataAddedMb * unitPrice.pricePerMB
//            return (false, getMaxBuildableDataMBs(), perc)
//        } else {
//            let perc = (desiredDataValue / maxDataInMB) * 100
//            if desiredDataValue < 0 {
//                self.dataAddedMb = 0
//            } else {
//                self.dataAddedMb = desiredDataValue
//            }
//            self.addedDataCharge = dataAddedMb * unitPrice.pricePerMB
//            return (true, desiredDataValue, perc)
//        }
//    }
    
//    func submitSMSIncrement(percentage: Double) -> (possible: Bool, addedValue: Double, percentage: Double) {
//
//        let desiredDataValue = maxSMSCount * percentage
//        if desiredDataValue > getMaxBuildableSMS() {
//            let perc = (getMaxBuildableSMS() / maxSMSCount) * 100
//            if getMaxBuildableSMS() < 0 {
//                self.smsAdded = 0
//            } else {
//                self.smsAdded = getMaxBuildableSMS()
//            }
//            self.addedSMSCharge = smsAdded * unitPrice.pricePerSMS
//            return (false, getMaxBuildableSMS(), perc)
//        } else {
//            let perc = (desiredDataValue / maxSMSCount) * 100
//            if desiredDataValue < 0 {
//                self.smsAdded = 0
//            } else {
//                self.smsAdded = desiredDataValue
//            }
//            self.smsAdded = desiredDataValue
//            self.addedSMSCharge = smsAdded * unitPrice.pricePerSMS
//            return (true, desiredDataValue, perc)
//        }
//    }
    
//    func submitVoiceIncrement(percentage: Double) -> (possible: Bool, addedValue: Double, percentage: Double) {
//
//        let desiredVoiceValue = maxVoiceInSecs * percentage
//        if desiredVoiceValue > getMaxBuildableVoiceSecs() {
//            let perc = (getMaxBuildableVoiceSecs() / maxVoiceInSecs) * 100
//            if getMaxBuildableVoiceSecs() < 0 {
//                self.voiceAddedSec = 0
//            } else {
//                self.voiceAddedSec = getMaxBuildableVoiceSecs()
//            }
//            self.addedVoiceCharge = (voiceAddedSec / 60) * unitPrice.pricePerMin
//            return (false, getMaxBuildableVoiceSecs(), perc)
//        } else {
//            let perc = (desiredVoiceValue / maxVoiceInSecs) * 100
//            if desiredVoiceValue < 0 {
//                self.voiceAddedSec = 0
//            } else {
//                self.voiceAddedSec = desiredVoiceValue
//            }
//            self.voiceAddedSec = desiredVoiceValue
//            self.addedVoiceCharge = (voiceAddedSec / 60) * unitPrice.pricePerMin
//            return (true, desiredVoiceValue, perc)
//        }
//    }
    
    func getCurrentDataValueInGB() -> Double {
        if self.dataObject == nil {
            return 0.0
        }
        return (self.dataAddedMb + (self.dataObject.available / 1024)) / 1024
    }
    
    func getCurrentSMSValue() -> Double {
        if self.smsObject == nil {
            return 0.0
        }
        return self.smsAdded + (self.smsObject.available)
    }
    
    func getCurrentVoiceValueInSec() -> Double {
        if self.voiceObject == nil {
            return 0.0
        }
        return self.voiceAddedSec + (self.voiceObject.available)
    }
    
    func getCurrentVoiceValueInMin() -> Double {
        return (self.voiceAddedSec + self.voiceObject.available) / 60
    }
    
    func getAddedDataValueInGB() -> Double {
        return (self.dataAddedMb / 1024)
    }
    
    func getAddedDataValueInGBOrMB() -> (value: Double, isGB: Bool) {
        if self.dataAddedMb > 1023 {
            return (value: dataAddedMb / 1024, isGB: true)
        } else {
            return (value: dataAddedMb, isGB: false)
        }
    }
    
    func getAddedDataValueInMB() -> Double {
        return self.dataAddedMb
    }
    
    func getAddedSMSValue() -> Double {
        return self.smsAdded
    }
    
    func getAddedVoiceValueInSec() -> Double {
        return self.voiceAddedSec
    }
    
    func getAddedVoiceValueInMin() -> Double {
        return self.voiceAddedSec / 60
    }
    
    func getAddedDataCharge() -> Double {
        return self.addedDataCharge
    }
    
    func getAddedSMSCharge() -> Double {
        return self.addedSMSCharge
    }
    
    func getAddedVoiceCharge() -> Double {
        return self.addedVoiceCharge
    }
    
    func getTotalRBSpent() -> Double {
        return self.addedDataCharge + self.addedVoiceCharge + self.addedSMSCharge
    }
    
    func getBalanceAfterPurchase() -> Double {
        return Double(self.userObject.rbBalance!)! - getTotalRBSpent()
    }
    
    func clearSession() {
        
        voiceAddedSec = 0
        smsAdded = 0
        dataAddedMb = 0
        
        addedVoiceCharge = 0
        addedSMSCharge = 0
        addedDataCharge = 0
    }
    
    func getBuiltValues() -> (data: Double, voice: Double, sms: Double) {
        return (data: self.dataAddedMb, voice: self.voiceAddedSec / 60, sms: self.smsAdded)
    }
    
    func shouldShowBuildView() -> Bool {
        if self.getTotalRBSpent() < 1 {
            return false
        } else {
            return true
        }
    }
    
    /* conversion implementation */
    
    /**
     This function returns the rounded RB balance down to the nearest zero.
     - parameter rule: FloatingPointRoundingRule used to round current RB value
     - returns: Rounded current RB value
     */
    func getRoundedRBBalance(rule: FloatingPointRoundingRule = .down) -> Double {
        return Double(self.userObject.rbBalance ?? "0")!.rounded(rule)
    }
    
    /**
     This function returns the buildable data amount with respective to the current RB balance rounded down to nearest zero, in MBs.
     Uses getRoundedRBBalance() function to calculate current RB balance.
     - returns: Maximum buildable data in MBs
     */
    func getMaxBuildableDataMBs() -> Double {
        
        let availableAmount = getRoundedRBBalance() - (self.addedSMSCharge + self.addedVoiceCharge)
        let buyableMBs = Double(availableAmount / unitPrice.pricePerMB)
        let mbsPerRB = 1 / unitPrice.pricePerMB
        let spendableRBsRounded = (buyableMBs / mbsPerRB).rounded(.down)
        return spendableRBsRounded * mbsPerRB
    }
    
    /**
     This function returns the buildable sms amount with respective to the current RB balance rounded down to nearest zero.
     Uses getRoundedRBBalance() function to calculate current RB balance.
     - returns: Maximum buildable sms.
     */
    func getMaxBuildableSMS() -> Double {
        
        let availableAmount = getRoundedRBBalance() - (self.addedDataCharge + self.addedVoiceCharge)
        let buyableSMSs = Double(availableAmount / unitPrice.pricePerSMS)
        let smsPerRB = 1 / unitPrice.pricePerSMS
        let spendableRBsRounded = (buyableSMSs / smsPerRB).rounded(.down)
        return spendableRBsRounded * smsPerRB
    }
    
    /**
     function definition
     */
    func getValidatedDataMBsWithCost(desiredValueMB: Double) -> (acceptedValue: Double, acceptedRBAmount: Double) {
        
        let costForDesiredData = desiredValueMB * unitPrice.pricePerMB
        let roundedCostForData = costForDesiredData.rounded(.down)
        let buyableMBs = roundedCostForData / unitPrice.pricePerMB
        return (buyableMBs, roundedCostForData)
    }
    
    /**
     function definition
     */
    func getValidatedSMSWithCost(desiredValue: Double) -> (acceptedValue: Double, acceptedRBAmount: Double) {
        
        let costForDesiredSMS = desiredValue * unitPrice.pricePerMB
        let roundedCostForSMS = costForDesiredSMS.rounded(.down)
        let buyableSMS = roundedCostForSMS / unitPrice.pricePerSMS
        return (buyableSMS, roundedCostForSMS)
    }
    
    /**
     function definition
     */
    func getValidatedVoiceWithCost(desiredValueSec: Double) -> (acceptedValue: Double, acceptedRBAmount: Double) {
        
        let costForDesiredVoice = (desiredValueSec / 60) * unitPrice.pricePerMin
        let roundedCostForVoice = costForDesiredVoice.rounded(.down)
        let buyableVoice = roundedCostForVoice / unitPrice.pricePerMin
        return (buyableVoice * 60, roundedCostForVoice)
    }
    
    func getMaxBuildableVoiceSecs() -> Double {
        
        let availableAmount = getRoundedRBBalance() - (self.addedSMSCharge + self.addedDataCharge)
        let buyableVoice = Double(availableAmount / unitPrice.pricePerMin)
        return buyableVoice * 60
    }
    
    
    
    func submitDataIncrement(percentage: Double) -> (possible: Bool, addedValue: Double, percentage: Double) {
        
        let desiredDataValue = maxDataInMB * percentage
        
        if desiredDataValue > getMaxBuildableDataMBs() {
            let perc = (getMaxBuildableDataMBs() / maxDataInMB) * 100
            if getMaxBuildableDataMBs() < 0 {
                self.dataAddedMb = 0
            } else {
                self.dataAddedMb = getMaxBuildableDataMBs()
            }
            self.addedDataCharge = dataAddedMb * unitPrice.pricePerMB
            return (false, getMaxBuildableDataMBs(), perc)
        } else {
            let perc = (desiredDataValue / maxDataInMB) * 100
            let val = self.getValidatedDataMBsWithCost(desiredValueMB: desiredDataValue)
            
            if desiredDataValue < 0 {
                self.dataAddedMb = 0
            } else {
                self.dataAddedMb = val.acceptedValue
            }
            self.addedDataCharge = val.acceptedRBAmount
            return (true, val.acceptedValue, perc)
        }
    }
    
    
    func submitSMSIncrement(percentage: Double) -> (possible: Bool, addedValue: Double, percentage: Double) {
        
        let desiredDataValue = maxSMSCount * percentage
        
        if desiredDataValue > getMaxBuildableSMS() {
            let perc = (getMaxBuildableSMS() / maxSMSCount) * 100
            if getMaxBuildableSMS() < 0 {
                self.smsAdded = 0
            } else {
                self.smsAdded = getMaxBuildableSMS()
            }
            self.addedSMSCharge = smsAdded * unitPrice.pricePerSMS
            return (false, getMaxBuildableSMS(), perc)
        } else {
            let perc = (desiredDataValue / maxSMSCount) * 100
            let val = self.getValidatedSMSWithCost(desiredValue: desiredDataValue)
            
            if desiredDataValue < 0 {
                self.smsAdded = 0
            } else {
                self.smsAdded = val.acceptedValue
            }
            self.addedSMSCharge = val.acceptedRBAmount
            return (true, val.acceptedRBAmount, perc)
        }
    }
    
    
    func submitVoiceIncrement(percentage: Double) -> (possible: Bool, addedValue: Double, percentage: Double) {
        
        let desiredVoiceValue = maxVoiceInSecs * percentage
        if desiredVoiceValue > getMaxBuildableVoiceSecs() {
            let perc = (getMaxBuildableVoiceSecs() / maxVoiceInSecs) * 100
            if getMaxBuildableVoiceSecs() < 0 {
                self.voiceAddedSec = 0
            } else {
                self.voiceAddedSec = getMaxBuildableVoiceSecs()
            }
            self.addedVoiceCharge = (voiceAddedSec / 60) * unitPrice.pricePerMin
            return (false, getMaxBuildableVoiceSecs(), perc)
        } else {
            let perc = (desiredVoiceValue / maxVoiceInSecs) * 100
            let val = self.getValidatedVoiceWithCost(desiredValueSec: desiredVoiceValue)
            
            if desiredVoiceValue < 0 {
                self.voiceAddedSec = 0
            } else {
                self.voiceAddedSec = val.acceptedValue
            }
            
            self.addedVoiceCharge = val.acceptedRBAmount
            return (true, val.acceptedRBAmount, perc)
        }
    }
    
    /* conversion implementation end */
}
