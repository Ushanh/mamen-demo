//
////  TopUpPresentationAnimator.swift
////  mamen
////
////  Created by Ushan Hattotuwa on 7/24/17.
////  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//class TopUpPresentationAnimator: NSObject, UIViewControllerAnimatedTransitioning {
//
//    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
//        return 1.5
//    }
//
//    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
//
//        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
//        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
//        let containerView = transitionContext.containerView
//
//        let animationDuration = self .transitionDuration(using: transitionContext)
//
//        // take a snapshot of the detail ViewController so we can do whatever with it (cause it's only a view), and don't have to care about breaking constraints
//        let snapshotView = toViewController.view.resizableSnapshotView(from: toViewController.view.frame, afterScreenUpdates: true, withCapInsets: UIEdgeInsets.zero)
//        snapshotView?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
//        snapshotView?.center = fromViewController.view.center
//        containerView.addSubview(snapshotView!)
//
//        // hide the detail view until the snapshot is being animated
//        toViewController.view.alpha = 0.0
//        containerView.addSubview(toViewController.view)
//
//        UIView.animate(withDuration: animationDuration, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 20.0, options: [],
//                                   animations: { () -> Void in
//                                    snapshotView?.transform = CGAffineTransform.identity
//        }, completion: { (finished) -> Void in
//            snapshotView?.removeFromSuperview()
//            toViewController.view.alpha = 1.0
//            transitionContext.completeTransition(finished)
//        })
//    }
//}
//
//class TransitionDelegate: NSObject, UIViewControllerTransitioningDelegate {
//
//    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        let presentationAnimator = TopUpPresentationAnimator()
//        return presentationAnimator
//    }
//
//    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        let dismissalAnimator = TransitionDismissalAnimator()
//        return dismissalAnimator
//    }
//}
//
//
//class TransitionDismissalAnimator: NSObject, UIViewControllerAnimatedTransitioning {
//
//    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
//        return 2
//    }
//
//    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
//        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)! as! WalletViewController
//        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
//        let containerView = transitionContext.containerView
//
//        let animationDuration = self.transitionDuration(using: transitionContext)
//
//        let snapshotView = fromViewController.addOnView.resizableSnapshotView(from: fromViewController.view.frame, afterScreenUpdates: true, withCapInsets: UIEdgeInsets.zero)
//        snapshotView?.center = toViewController.view.center
//        containerView.addSubview(snapshotView!)
//
//        fromViewController.view.alpha = 0.0
//
//        let toViewControllerSnapshotView = toViewController.view.resizableSnapshotView(from: toViewController.view.frame, afterScreenUpdates: true, withCapInsets: UIEdgeInsets.zero)
//        containerView.insertSubview(toViewControllerSnapshotView!, belowSubview: snapshotView!)
//
//        UIView.animate(withDuration: animationDuration, animations: { () -> Void in
//            snapshotView?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
//            snapshotView?.alpha = 0.0
//        }) { (finished) -> Void in
//            toViewControllerSnapshotView?.removeFromSuperview()
//            snapshotView?.removeFromSuperview()
//            fromViewController.view.removeFromSuperview()
//            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
//        }
//    }
//}
//
//class FlipPresentAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
//
//    var originFrame = CGRect.zero
//
//    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
//        return 0.6
//    }
//
//    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
//
//        guard let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
//            let containerView = transitionContext.containerView,
//            let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) else {
//                return
//        }
//
//        let initialFrame = originFrame
//        let finalFrame = transitionContext.finalFrameForViewController(toVC)
//
//        let snapshot = toVC.view.snapshotViewAfterScreenUpdates(true)
//
//        snapshot.frame = initialFrame
//        snapshot.layer.cornerRadius = 25
//        snapshot.layer.masksToBounds = true
//
//        containerView.addSubview(toVC.view)
//        containerView.addSubview(snapshot)
//        toVC.view.hidden = true
//
//        AnimationHelper.perspectiveTransformForContainerView(containerView)
//
//        snapshot.layer.transform = AnimationHelper.yRotation(M_PI_2)
//
//        let duration = transitionDuration(transitionContext)
//
//        UIView.animateKeyframesWithDuration(
//            duration,
//            delay: 0,
//            options: .CalculationModeCubic,
//            animations: {
//
//                UIView.addKeyframeWithRelativeStartTime(0.0, relativeDuration: 1/3, animations: {
//                    fromVC.view.layer.transform = AnimationHelper.yRotation(-M_PI_2)
//                })
//
//                UIView.addKeyframeWithRelativeStartTime(1/3, relativeDuration: 1/3, animations: {
//                    snapshot.layer.transform = AnimationHelper.yRotation(0.0)
//                })
//
//                UIView.addKeyframeWithRelativeStartTime(2/3, relativeDuration: 1/3, animations: {
//                    snapshot.frame = finalFrame
//                })
//        },
//            completion: { _ in
//                toVC.view.hidden = false
//                snapshot.removeFromSuperview()
//                transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
//        })
//    }
//}

import UIKit


class CustomPresentationAnimationController: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    let isPresenting :Bool
    let duration :TimeInterval = 0.5
    
    init(isPresenting: Bool) {
        self.isPresenting = isPresenting
        
        super.init()
    }
    
//    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//
//    }
//
//    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//
//    }
    
    
    // ---- UIViewControllerAnimatedTransitioning methods
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return self.duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning)  {
        if isPresenting {
            animatePresentationWithTransitionContext(transitionContext: transitionContext)
        }
        else {
            animateDismissalWithTransitionContext(transitionContext: transitionContext)
        }
    }
    
    
    // ---- Helper methods
    func animatePresentationWithTransitionContext(transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let presentedController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
            let presentedControllerView = transitionContext.view(forKey: UITransitionContextViewKey.to),
            let containerView: UIView = transitionContext.containerView
            else {
                return
        }
        
        // Position the presented view off the top of the container view
        presentedControllerView.frame = transitionContext.finalFrame(for: presentedController)
        presentedControllerView.center.y -= containerView.bounds.size.height
        
        containerView.addSubview(presentedControllerView)
        
        // Animate the presented view to it's final position
        UIView.animate(withDuration: self.duration, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.0, options: .allowUserInteraction, animations: {
            presentedControllerView.center.y += containerView.bounds.size.height
        }, completion: {(completed: Bool) -> Void in
            transitionContext.completeTransition(completed)
        })
    }
    
    func animateDismissalWithTransitionContext(transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let presentedControllerView = transitionContext.view(forKey: UITransitionContextViewKey.from),
            let containerView: UIView = transitionContext.containerView
            else {
                return
        }
        
        // Animate the presented view off the bottom of the view
        UIView.animate(withDuration: self.duration, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.0, options: .allowUserInteraction, animations: {
            presentedControllerView.center.y += containerView.bounds.size.height
        }, completion: {(completed: Bool) -> Void in
            transitionContext.completeTransition(completed)
        })
    }
}

