
//
//  BaseTableView.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 6/29/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import UIKit

class BaseTableView: UITableView {
    
    //MARK: Touches began
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }

}
