//
//  SplashView.swift
//  Exported from Kite Compositor for Mac 1.4
//
//  Created on 6/14/17, 2:41 PM.
//


import UIKit

class SplashView: UIView
{

    // MARK: - Initialization

    init()
    {
        super.init(frame: CGRect(x: 0, y: 0, width: 375, height: 667))
        self.setupLayers()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.setupLayers()
    }

    // MARK: - Setup Layers

    private func setupLayers()
    {
        // Images
        //
        guard let landingLaunchPngImage = UIImage(named: "LANDING-00---Launch.png") else {
            fatalError("Warning: Unable to locate image named 'LANDING-00---Launch.png'")
        }

        guard let rectanglePngImage = UIImage(named: "Rectangle.png") else {
            fatalError("Warning: Unable to locate image named 'Rectangle.png'")
        }

        guard let rectangleCopyPngImage = UIImage(named: "Rectangle-Copy-2.png") else {
            fatalError("Warning: Unable to locate image named 'Rectangle-Copy-2.png'")
        }

        guard let rectangleCopyPngImage1 = UIImage(named: "Rectangle-Copy.png") else {
            fatalError("Warning: Unable to locate image named 'Rectangle-Copy.png'")
        }

        guard let poweredByDhiraaguPngImage = UIImage(named: "POWERED-BY-DHIRAAGU.png") else {
            fatalError("Warning: Unable to locate image named 'POWERED-BY-DHIRAAGU.png'")
        }

        guard let logoPngImage = UIImage(named: "logo-1.png") else {
            fatalError("Warning: Unable to locate image named 'logo-1.png'")
        }

        // Colors
        //
        let backgroundcolor = UIColor.white
        let fillcolor = UIColor(red: 0.847, green: 0.847, blue: 0.847, alpha: 1)
        let strokecolor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        let fillcolor1 = UIColor(red: 0.533333, green: 0.454902, blue: 0.941176, alpha: 1)

        // splash
        //
        let splashLayer = CALayer()
        splashLayer.name = "splash"
        splashLayer.bounds = CGRect(x: 0, y: 0, width: 375, height: 667)
        splashLayer.position = CGPoint(x: 0, y: 0)
        splashLayer.anchorPoint = CGPoint(x: 0, y: 0)
        splashLayer.contentsGravity = kCAGravityCenter
        splashLayer.masksToBounds = true
        splashLayer.backgroundColor = backgroundcolor.cgColor

            // splash Sublayers
            //

            // LANDING 00 - Launch
            //
            let landingLaunchLayer = CALayer()
            landingLaunchLayer.name = "LANDING 00 - Launch"
            landingLaunchLayer.bounds = CGRect(x: 0, y: 0, width: 375, height: 667)
            landingLaunchLayer.position = CGPoint(x: 0, y: 0)
            landingLaunchLayer.anchorPoint = CGPoint(x: 0, y: 0)
            landingLaunchLayer.contents = landingLaunchPngImage.cgImage
            landingLaunchLayer.contentsGravity = kCAGravityCenter
            landingLaunchLayer.isHidden = true

            splashLayer.addSublayer(landingLaunchLayer)

            // Rectangle
            //
            let rectangleLayer = CAShapeLayer()
            rectangleLayer.name = "Rectangle"
            rectangleLayer.bounds = CGRect(x: 0, y: 0, width: 629, height: 629)
            rectangleLayer.position = CGPoint(x: -16, y: -388)
            rectangleLayer.anchorPoint = CGPoint(x: 0, y: 0)
            rectangleLayer.contents = rectanglePngImage.cgImage
            rectangleLayer.contentsGravity = kCAGravityCenter

                // Rectangle Animations
                //

                // position.y
                //
                let positionYAnimation = CABasicAnimation()
                positionYAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 1.43559
                positionYAnimation.duration = 2.128887
                positionYAnimation.fillMode = kCAFillModeBackwards
                positionYAnimation.isRemovedOnCompletion = false
                positionYAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
                positionYAnimation.keyPath = "position.y"
                positionYAnimation.toValue = -388
                positionYAnimation.fromValue = -616

                rectangleLayer.add(positionYAnimation, forKey: "positionYAnimation")
            rectangleLayer.fillColor = fillcolor.cgColor
            rectangleLayer.strokeColor = strokecolor.cgColor
            rectangleLayer.fillRule = kCAFillRuleEvenOdd
            rectangleLayer.lineWidth = 0

            splashLayer.addSublayer(rectangleLayer)

            // Rectangle Copy 2
            //
            let rectangleCopyLayer = CAShapeLayer()
            rectangleCopyLayer.name = "Rectangle Copy 2"
            rectangleCopyLayer.bounds = CGRect(x: 0, y: 0, width: 445, height: 445)
            rectangleCopyLayer.position = CGPoint(x: 275, y: 530)
            rectangleCopyLayer.anchorPoint = CGPoint(x: 0, y: 0)
            rectangleCopyLayer.contents = rectangleCopyPngImage.cgImage
            rectangleCopyLayer.contentsGravity = kCAGravityCenter

                // Rectangle Copy 2 Animations
                //

                // position.x
                //
                let positionXAnimation = CABasicAnimation()
                positionXAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 1.143251
                positionXAnimation.duration = 2.214688
                positionXAnimation.fillMode = kCAFillModeBackwards
                positionXAnimation.isRemovedOnCompletion = false
                positionXAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
                positionXAnimation.keyPath = "position.x"
                positionXAnimation.toValue = 275
                positionXAnimation.fromValue = 401

                rectangleCopyLayer.add(positionXAnimation, forKey: "positionXAnimation")
            rectangleCopyLayer.fillColor = fillcolor1.cgColor
            rectangleCopyLayer.strokeColor = strokecolor.cgColor
            rectangleCopyLayer.fillRule = kCAFillRuleEvenOdd
            rectangleCopyLayer.lineWidth = 0

            splashLayer.addSublayer(rectangleCopyLayer)

            // Rectangle Copy
            //
            let rectangleCopyLayer1 = CAShapeLayer()
            rectangleCopyLayer1.name = "Rectangle Copy"
            rectangleCopyLayer1.bounds = CGRect(x: 0, y: 0, width: 629, height: 629)
            rectangleCopyLayer1.position = CGPoint(x: -481, y: 235)
            rectangleCopyLayer1.anchorPoint = CGPoint(x: 0, y: 0)
            rectangleCopyLayer1.contents = rectangleCopyPngImage1.cgImage
            rectangleCopyLayer1.contentsGravity = kCAGravityCenter

                // Rectangle Copy Animations
                //

                // position.x
                //
                let positionXAnimation1 = CABasicAnimation()
                positionXAnimation1.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.785312
                positionXAnimation1.duration = 2.214688
                positionXAnimation1.fillMode = kCAFillModeBackwards
                positionXAnimation1.isRemovedOnCompletion = false
                positionXAnimation1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
                positionXAnimation1.keyPath = "position.x"
                positionXAnimation1.toValue = -481
                positionXAnimation1.fromValue = -652

                rectangleCopyLayer1.add(positionXAnimation1, forKey: "positionXAnimation1")
            rectangleCopyLayer1.fillColor = fillcolor.cgColor
            rectangleCopyLayer1.strokeColor = strokecolor.cgColor
            rectangleCopyLayer1.fillRule = kCAFillRuleEvenOdd
            rectangleCopyLayer1.lineWidth = 0

            splashLayer.addSublayer(rectangleCopyLayer1)

            // POWERED BY DHIRAAGU
            //
            let poweredByDhiraaguLayer = CALayer()
            poweredByDhiraaguLayer.name = "POWERED BY DHIRAAGU"
            poweredByDhiraaguLayer.bounds = CGRect(x: 0, y: 0, width: 155, height: 15)
            poweredByDhiraaguLayer.position = CGPoint(x: 110, y: 625)
            poweredByDhiraaguLayer.anchorPoint = CGPoint(x: 0, y: 0)
            poweredByDhiraaguLayer.contents = poweredByDhiraaguPngImage.cgImage
            poweredByDhiraaguLayer.contentsGravity = kCAGravityCenter
            poweredByDhiraaguLayer.magnificationFilter = kCAFilterNearest
            poweredByDhiraaguLayer.minificationFilter = kCAFilterNearest

                // POWERED BY DHIRAAGU Animations
                //

                // opacity
                //
                let opacityAnimation = CABasicAnimation()
                opacityAnimation.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
                opacityAnimation.duration = 2.000022
                opacityAnimation.fillMode = kCAFillModeBackwards
                opacityAnimation.isRemovedOnCompletion = false
                opacityAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                opacityAnimation.keyPath = "opacity"
                opacityAnimation.toValue = 1
                opacityAnimation.fromValue = 0

                poweredByDhiraaguLayer.add(opacityAnimation, forKey: "opacityAnimation")

            splashLayer.addSublayer(poweredByDhiraaguLayer)

            // logo
            //
            let logoLayer = CALayer()
            logoLayer.name = "logo"
            logoLayer.bounds = CGRect(x: 0, y: 0, width: 180, height: 56)
            logoLayer.position = CGPoint(x: 98, y: 309)
            logoLayer.anchorPoint = CGPoint(x: 0, y: 0)
            logoLayer.contents = logoPngImage.cgImage
            logoLayer.contentsGravity = kCAGravityCenter

                // logo Animations
                //

                // opacity
                //
                let opacityAnimation1 = CABasicAnimation()
                opacityAnimation1.beginTime = self.layer.convertTime(CACurrentMediaTime(), from: nil) + 0.000001
                opacityAnimation1.duration = 2.000022
                opacityAnimation1.fillMode = kCAFillModeBackwards
                opacityAnimation1.isRemovedOnCompletion = false
                opacityAnimation1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                opacityAnimation1.keyPath = "opacity"
                opacityAnimation1.toValue = 1
                opacityAnimation1.fromValue = 0

                logoLayer.add(opacityAnimation1, forKey: "opacityAnimation1")

            splashLayer.addSublayer(logoLayer)

        self.layer.addSublayer(splashLayer)

    }

    // MARK: - Responder

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        guard let location = touches.first?.location(in: self.superview),
              let hitLayer = self.layer.presentation()?.hitTest(location) else { return }

        print("Layer \(hitLayer.name ?? String(describing: hitLayer)) was tapped.")
    }
}
