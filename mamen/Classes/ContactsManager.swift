//
//  ContactsManager.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 8/21/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import Contacts

class ContactsFetchError {
    var id: String!
    var message: String!
    
    init(id: String, message: String) {
        self.id = id
        self.message = message
    }
}

class ContactsManager {
    
    static func getPhonebookContacts() -> (friends: [MamenFriend]?, errors: ContactsFetchError?) {
        
        var contacts = [MamenFriend]()
        let contactsStore = CNContactStore()
        var fetchError: ContactsFetchError?
        
        ///
        
        let keysToFetch = [CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey, CNContactMiddleNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey, CNContactImageDataAvailableKey]
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactsStore.containers(matching: nil)
        } catch {
            fetchError = ContactsFetchError(id: "1", message: "Falied to fetch contacts from your phonebook. Please grant access")
        }
        
        var results: [CNContact] = []
        
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactsStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch {
                fetchError = ContactsFetchError(id: "1", message: "Falied to fetch contacts from your phonebook. Please grant access")
            }
        }
        
        for result in results {
            let name = self.getNameFromContact(contact: result)
            if name.count > 0 {
                let tempContact = MamenFriend(contact: result, name: name, isFriend: false, friendObject: nil)
                contacts.append(tempContact)
            }
        }
        
        return (contacts, fetchError)
    }
    
    static func getNameFromContact(contact: CNContact) -> String {
        
        if contact.givenName != "" {
            return contact.givenName
        } else if contact.middleName != "" {
            return contact.middleName
        } else if contact.familyName != "" {
            return contact.familyName
        } else {
            return ""
        }
    }
}
