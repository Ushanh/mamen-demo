//
//  PercentageContainerCell.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 7/16/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import UIKit
import MKRingProgressView

@IBDesignable
class PercentageContainerView: UIView {
    
    var outerCircleLayer: CAShapeLayer!
    var middleCircleLayer: CAShapeLayer!
    var innerCircleLayer: CAShapeLayer!
    
    var outerFillLayer: CAShapeLayer?
    var middleFillLayer: CAShapeLayer?
    var innerFillLayer: CAShapeLayer?
    
    var outerFillPath: UIBezierPath!
    
    var ringGroupView: MKRingProgressGroupView?
    
    @IBInspectable var outerLineColor: UIColor = UIColor(red:0.91, green:0.97, blue:1.00, alpha:1.0) {
        didSet {
            
        }
    }
    
    @IBInspectable var middleLineColor: UIColor = UIColor(red:1.00, green:0.92, blue:0.92, alpha:1.0) {
        didSet {
            
        }
    }
    
    @IBInspectable var innerLineColor: UIColor = UIColor(red:0.93, green:0.98, blue:0.89, alpha:1.0) {
        didSet {
            
        }
    }
    
    @IBInspectable var lineWidth: CGFloat = 0
    
    override func draw(_ rect: CGRect) {
        
        //outerCircleLayer?.removeFromSuperlayer()
        //middleCircleLayer?.removeFromSuperlayer()
        //innerCircleLayer?.removeFromSuperlayer()
        
        //outerCircleLayer = nil
        //middleCircleLayer = nil
        //innerCircleLayer = nil
        
        if outerCircleLayer == nil {
            outerCircleLayer = CAShapeLayer()
            outerCircleLayer.path = UIBezierPath(roundedRect: CGRect(x: 10, y: 10, width: self.frame.size.width - 20, height: self.frame.size.height - 20), cornerRadius: self.frame.size.width / 2).cgPath
            outerCircleLayer.position = CGPoint(x: 0, y: 0)
            outerCircleLayer.strokeColor = outerLineColor.cgColor
            outerCircleLayer.lineWidth = lineWidth
            outerCircleLayer.fillColor = UIColor.clear.cgColor
            self.layer.addSublayer(outerCircleLayer)
        }
        
        if middleCircleLayer == nil {
            middleCircleLayer = CAShapeLayer()
            middleCircleLayer.path = UIBezierPath(roundedRect: CGRect(x: 30, y: 30, width: self.frame.size.width - 60, height: self.frame.size.height - 60), cornerRadius: self.frame.size.width / 2).cgPath
            middleCircleLayer.position = CGPoint(x: 0, y: 0)
            middleCircleLayer.strokeColor = middleLineColor.cgColor
            middleCircleLayer.lineWidth = lineWidth
            middleCircleLayer.fillColor = UIColor.clear.cgColor
            self.layer.addSublayer(middleCircleLayer)
        }
        
        if innerCircleLayer == nil {
            innerCircleLayer = CAShapeLayer()
            innerCircleLayer.path = UIBezierPath(roundedRect: CGRect(x: 50, y: 50, width: self.frame.size.width - 100, height: self.frame.size.height - 100), cornerRadius: self.frame.size.width / 2).cgPath
            innerCircleLayer.position = CGPoint(x: 0, y: 0)
            innerCircleLayer.strokeColor = innerLineColor.cgColor
            innerCircleLayer.lineWidth = lineWidth
            innerCircleLayer.fillColor = UIColor.clear.cgColor
            self.layer.addSublayer(innerCircleLayer)
        }
        
        self.setupFillViews()
        //self.animateFilling()
    }
    
    func setupFillViews() {
        
        if self.ringGroupView != nil {
            self.ringGroupView!.removeFromSuperview()
            self.ringGroupView = nil
        }
        
        self.ringGroupView = MKRingProgressGroupView(frame: CGRect(x:0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
        
        ringGroupView?.ring1.startColor = UIColor(red:0.18, green:0.60, blue:0.98, alpha:1.0)
        ringGroupView?.ring1.endColor = UIColor(red:0.15, green:0.87, blue:0.99, alpha:1.0)
        ringGroupView?.ring1.ringWidth = 15
        //ringGroupView?.ring1.progress = 0
        ringGroupView?.ring1.backgroundRingColor = UIColor.clear
        ringGroupView?.ring1.shadowOpacity = 0.1
        
        ringGroupView?.ring2.startColor = UIColor(red:0.98, green:0.32, blue:0.33, alpha:1.0)
        ringGroupView?.ring2.endColor = UIColor(red:0.97, green:0.82, blue:0.24, alpha:1.0)
        ringGroupView?.ring2.ringWidth = 15
        //ringGroupView?.ring2.progress = 0
        ringGroupView?.ring2.backgroundRingColor = UIColor.clear
        ringGroupView?.ring2.shadowOpacity = 0.1
        
        ringGroupView?.ring3.startColor = UIColor(red:0.41, green:0.87, blue:0.16, alpha:1.0)
        ringGroupView?.ring3.endColor = UIColor(red:0.82, green:0.91, blue:0.20, alpha:1.0)
        ringGroupView?.ring3.ringWidth = 15
        //ringGroupView?.ring3.progress = 0
        ringGroupView?.ring3.backgroundRingColor = UIColor.clear
        ringGroupView?.ring3.shadowOpacity = 0.1
        
        self.addSubview(ringGroupView!)
    }
    
    func resetFilling(dataPercentage: Double, voicePercentage: Double, smsPercentage: Double) {
        
        DispatchQueue.main.async {
            self.ringGroupView?.ring1.progress = 0
            self.ringGroupView?.ring2.progress = 0
            self.ringGroupView?.ring3.progress = 0
        }
    }
    
    func animateFilling(dataPercentage: Double, voicePercentage: Double, smsPercentage: Double) {
    
        DispatchQueue.main.async {
            CATransaction.begin()
            CATransaction.setAnimationDuration(1.0)
            self.ringGroupView?.ring1.progress = dataPercentage
            self.ringGroupView?.ring2.progress = voicePercentage
            self.ringGroupView?.ring3.progress = smsPercentage
            CATransaction.commit()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
