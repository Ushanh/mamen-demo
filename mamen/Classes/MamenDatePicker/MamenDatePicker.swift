//
//  MamenDatePicker.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 9/12/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import UIKit

protocol MamenDatePickerDelegate {
    
    func datePickerDidFinishSelection(date: Date)
    func datePickerDidCancel()
}

class MamenDatePicker: UIView {
    
    //MARK: Properties
    @IBOutlet var blurView: UIVisualEffectView!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var pickerContainerView: UIView!
    @IBOutlet var pickerContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet var pickerContainerHeightConstraint: NSLayoutConstraint!
    
    //MARK: Properties
    var delegate: MamenDatePickerDelegate? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = Bundle.main.loadNibNamed("MamenDatePicker", owner: self, options: [:])?.first as! MamenDatePicker
        view.frame = frame
        self.addSubview(view)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
    }
    
    func presentViewWithAnimation() {
        self.blurView.alpha = 0
        self.datePicker.alpha = 0
        self.pickerContainerBottomConstraint.constant = -(self.pickerContainerHeightConstraint.constant)
        self.layoutIfNeeded()
        self.pickerContainerBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseIn, animations: {
            self.datePicker.alpha = 0.5
            self.blurView.alpha = 0.4
            self.layoutIfNeeded()
        }, completion: { (comp) in
            
        })
    }
    
    func dismissViewWithAnimation() {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.pickerContainerBottomConstraint.constant = -(self.pickerContainerHeightConstraint.constant)
            self.layoutIfNeeded()
        }) { (completed) in
            UIView.animate(withDuration: 0.3, animations: {
                self.blurView.alpha = 0
                self.datePicker.alpha = 0
            }, completion: { (completion) in
                
            })
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.8) {
            self.removeFromSuperview()
        }
    }
    
    //MARK: Actions
    @IBAction func cancelButtonDidPress(_ sender: Any) {
        if  delegate != nil {
            delegate?.datePickerDidCancel()
            dismissViewWithAnimation()
        }
    }
    
    @IBAction func doneButtonDidPress(_ sender: Any) {
        if  delegate != nil {
            delegate?.datePickerDidFinishSelection(date: self.datePicker.date)
            dismissViewWithAnimation()
        }
    }
}

extension UIViewController {
    
    func presentDatePicker(delegate: MamenDatePickerDelegate, currentDate: Date?) {
        let pickerView = MamenDatePicker(frame: self.view.frame)
        pickerView.delegate = delegate
        pickerView.datePicker.setValue(UIColor.darkGray, forKey: "textColor")
        pickerView.datePicker.performSelector(inBackground: Selector(("setHighlightsToday:")), with:UIColor.darkGray)
        pickerView.datePicker.maximumDate = Date().addingTimeInterval(-1 * (60 * 60 * 24))
        if currentDate != nil {
            pickerView.datePicker.date = currentDate!
        }
        self.view.addSubview(pickerView)
        pickerView.presentViewWithAnimation()
    }
}
