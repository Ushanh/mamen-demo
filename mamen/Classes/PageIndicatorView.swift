//
//  PageIndicatorView.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 6/14/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import UIKit

class PageIndicatorView: UIView {

    override func draw(_ rect: CGRect) {
        //Drawing code
        self.clipsToBounds = true
        self.layer.cornerRadius = rect.height/2
        print(rect)
    }
    
}
