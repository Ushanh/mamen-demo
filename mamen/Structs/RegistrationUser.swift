//
//  UserRegistration.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 8/16/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import ObjectMapper

struct RegistrationUser {
    
    var name: String!
    var avatar: UIImage?
    var email: String?
    var username: String!
    var location: String!
    var pid: String!
    var gender: String!
    var birthday: Date!
    var pidImage: UIImage?
    var mobile: String!
    
    func getJSONObject() -> NSMutableDictionary {
        let userDict = NSMutableDictionary()
        userDict.setValue(self.name, forKey: "name")
        //userDict.setValue(self.name, forKey: "avatar")
        userDict.setValue(self.username, forKey: "username")
        userDict.setValue(self.location, forKey: "location")
        userDict.setValue(self.pid, forKey: "pid")
        userDict.setValue(self.gender, forKey: "gender")
        userDict.setValue(self.birthday.toDobString(), forKey: "birthday")
        userDict.setValue(self.email ?? "", forKey: "email")
        //userDict.setValue(self.name, forKey: "pid_image")
        userDict.setValue(self.mobile, forKey: "mobile")
        return userDict
    }
}

