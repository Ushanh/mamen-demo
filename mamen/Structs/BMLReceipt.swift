//
//  BMLReceipt.swift
//  
//
//  Created by Ushan Hattotuwa on 9/20/17.
//

import Foundation

struct BMLReceiptKeys {
    // transaction no
    static var receiptNo        = "ctl00_ContentPlaceHolder1_lbl_InvoiceNo"// ==
    static var paymentNo        = "ctl00_ContentPlaceHolder1_lbl_payNo"//
    static var transactionNo    = "ctl00_ContentPlaceHolder1_lbl_OrderNo"
    static var transactionDate  = "ctl00_ContentPlaceHolder1_lbl_date" // ==
    static var accountNo        = "ctl00_ContentPlaceHolder1_lbl_ServiceNo"// == recharfe serv no
    static var billReferenceNo  = "ctl00_ContentPlaceHolder1_lbl_billref"//
    static var itemDescription  = "ctl00_ContentPlaceHolder1_ltr_Item_Desc"//
    static var amountIncGST     = "ctl00_ContentPlaceHolder1_lbl_amount"
    static var paymentStatus    = "ctl00_ContentPlaceHolder1_lbl_status" // ==
    static var appRefNo         = "ctl00_ContentPlaceHolder1_lbl_App_RefNo"// transact n0
}

struct BMLReceipt {
    
    var receiptNo       : String?
    var paymentNo       : String?
    var transactionNo   : String?
    var transactionDate : String?
    var accountNo       : String?
    var billReferenceNo : String?
    var itemDescription : String?
    var amountIncGST    : String?
    var paymentStatus   : String?
    var appRefNo        : String?
    
    init() {
        
    }
}
