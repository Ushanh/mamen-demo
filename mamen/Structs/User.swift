//
//  User.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 8/18/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import UIKit
import Realm
import RealmSwift
import ObjectMapper

struct User: Mappable {
    
    var name: String!
    var avatar: UIImage?
    var email: String?
    var username: String!
    var location: String!
    var pid: String!
    var gender: String!
    var birthday: Date!
    var mobile: String!
    
    init(name: String, avatar: UIImage?, email: String?, username: String, location: String, pid: String, gender: String, birthday: Date, mobile: String) {
        
        self.name = name
        self.avatar = avatar
        self.email = email
        self.username = NSUserName()
        self.location = location
        self.pid = pid
        self.gender = gender
        self.birthday = birthday
        self.mobile = mobile
    }
    
    init?(map: Map) {
        
    }
    
    func getJSONObject() -> NSMutableDictionary {
        let userDict = NSMutableDictionary()
        userDict.setValue(self.name, forKey: "name")
        userDict.setValue(self.username, forKey: "username")
        userDict.setValue(self.location, forKey: "location")
        userDict.setValue(self.pid, forKey: "pid")
        userDict.setValue(self.gender, forKey: "gender")
        userDict.setValue(self.birthday.toDobString(), forKey: "birthday")
        userDict.setValue(self.mobile, forKey: "mobile")
        return userDict
    }
    
    mutating func mapping(map: Map) {
        
        name <- map["name"]
        avatar <- map["avatar"]
        email <- map["email"]
        username <- map["username"]
        location <- map["location"]
        pid <- map["p_id"]
        gender <- map["gender"]
        birthday <- map["birthday"]
        mobile <- map["mobile"]
    }
}
