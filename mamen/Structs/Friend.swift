//
//  Friend.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 8/18/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import ObjectMapper
import Contacts

struct Friend: Mappable {
    
    var id: Int!
    var name: String!
    var username: String!
    var avatar: String?
    var mobile: String!
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        username <- map["username"]
        avatar <- map["avatar"]
        mobile <- map["mobile"]
    }
}

struct MamenFriend {
    
    var contact: CNContact!
    var name: String!
    var isFriend: Bool
    var friendObject: Friend?
    
    mutating func setFriendObject(friend: Friend) {
        self.friendObject = friend
        self.isFriend = true
    }
    
}
