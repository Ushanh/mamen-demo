//
//  Color.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 6/23/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import UIKit

struct Color {
    static var so2: UIColor = UIColor(red:0.40, green:0.41, blue:0.49, alpha:1.0)
    static var so7: UIColor = UIColor(red:0.00, green:0.88, blue:0.50, alpha:1.0)
    static var so8: UIColor = UIColor(red:0.15, green:0.86, blue:0.27, alpha:1.0)
    static var so12: UIColor = UIColor(red:0.90, green:0.00, blue:0.00, alpha:1.0)
    static var lightGray: UIColor = UIColor(red:0.81, green:0.85, blue:0.87, alpha:1.0)
}



struct MamenRowColor {
    
    var barGradientStart: UIColor
    var barGradientEnd: UIColor
    var percentageLabelColor: UIColor
    var leftIcon: UIImage
    var rightIcon: UIImage
    
    
    
}
