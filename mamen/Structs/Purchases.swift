//
//  Purchases.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 7/18/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import UIKit

struct Purchase {
    
    var type: Transmission!
    var currentValue: Float!
    var maxValue: Float!
    var minValue: Float!
    var amountPerUnit: Float!
    var initialValue: Float!
    var balance: Float!
    
    init(type: Transmission, currentValue: Float = 0, maxValue: Float, minValue: Float = 0, unitAmount: Float = 1, initialValue: Float, balance: Float) {
        self.type = type
        self.currentValue = currentValue
        self.maxValue = maxValue
        self.minValue = minValue
        self.amountPerUnit = unitAmount
        self.initialValue = initialValue
        self.balance = balance
    }
}

class Builds {
    
    var data = Purchase(type: .data, currentValue: 0, maxValue: 10, minValue: 0, unitAmount: 1, initialValue: 0, balance: 4)
    var voice = Purchase(type: .voice, currentValue: 0, maxValue: 300, minValue: 0, unitAmount: 1, initialValue: 0, balance: 100)
    var text = Purchase(type: .text, currentValue: 0, maxValue: 300, minValue: 0, unitAmount: 1, initialValue: 0, balance: 100)
    
    init() {
        
    }
    
    static let shared = Builds()
    
    func getAllTypes() -> (data: Purchase, voice: Purchase, text: Purchase) {
        return (data: data, voice: voice, text: text)
    }
    
    func getData() -> Purchase {
        return data
    }
    
    func getText() -> Purchase {
        return text
    }
    
    func getVoice() -> Purchase {
        return voice
    }
    
    func getCurrentValue(transmission: Transmission) -> Float {
        switch transmission {
        case .data:
            return Builds.shared.getData().currentValue
        case .text:
            return Builds.shared.getText().currentValue
        case .voice:
            return Builds.shared.getVoice().currentValue
        }
    }
    
    func clearAllTypes() {
        self.data = Purchase(type: .data, currentValue: 0, maxValue: 6, minValue: 0, unitAmount: 1, initialValue: 0, balance: 4)
        self.voice = Purchase(type: .voice, currentValue: 0, maxValue: 300, minValue: 0, unitAmount: 1, initialValue: 0, balance: 100)
        self.text = Purchase(type: .text, currentValue: 0, maxValue: 300, minValue: 0, unitAmount: 1, initialValue: 0, balance: 100)
    }
    
    func hasChangedFromInitialState() -> Bool {
        
        let dataChanged = self.data.initialValue != self.data.currentValue
        let voiceChanged = self.voice.initialValue != self.voice.currentValue
        let textChanged = self.text.initialValue != self.text.currentValue
        
        if dataChanged || voiceChanged || textChanged {
            return true
        } else {
            return false
        }
    }
    
    func getTotalAmount() -> Float {
        
        var totalAmount: Float = 0
        let dataAmount = data.currentValue * data.amountPerUnit
        let textAmount = text.currentValue * text.amountPerUnit
        let voiceAmount = voice.currentValue * voice.amountPerUnit
        totalAmount = dataAmount + textAmount + voiceAmount
        return totalAmount
    }
    
    func changeValue(transmission: Transmission, percentage: Float) {
        switch transmission {
            
        case .data:
            let value = ((self.data.maxValue - self.data.minValue) / 100) * percentage
            self.data.currentValue = value
            break
        case .voice:
            let value = ((self.voice.maxValue - self.voice.minValue) / 100) * percentage
            self.voice.currentValue = value
            break
        case .text:
            let value = ((self.text.maxValue - self.text.minValue) / 100) * percentage
            self.text.currentValue = value
            break
        }
    }
}


class TakeBacks {
    
    var data = Purchase(type: .data, currentValue: 0, maxValue: 6, minValue: 0, unitAmount: 1, initialValue: 0, balance: 0)
    var voice = Purchase(type: .voice, currentValue: 0, maxValue: 100, minValue: 0, unitAmount: 1, initialValue: 0, balance: 0)
    var text = Purchase(type: .text, currentValue: 0, maxValue: 100, minValue: 0, unitAmount: 1, initialValue: 0, balance: 0)
    
    init() {
        
    }
    
    let shared = TakeBacks()
    
    func getAllTypes() -> (data: Purchase, voice: Purchase, text: Purchase) {
        return (data: data, voice: voice, text: text)
    }
}


