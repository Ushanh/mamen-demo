//
//  Addon.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 6/14/19.
//  Copyright © 2019 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import ObjectMapper

//{
//    data =     {
//        billingFormat = 1;
//        bundleGroup =         (
//            {
//                bundles =                 (
//                    {
//                        bundleId = 777558371;
//                        bundleName = "Mamen Add-on 3GB";
//                        bundlePrice = 250;
//                        bundleSystemName = "MamenDataAddon_3GB_BNDL";
//                        bundleType = 8;
//                        expiresIn = 720;
//                        fundAvailable = 0;
//                        grantAmount = 3221225472;
//                        isRecurring = 0;
//                        purchasePrice = 250;
//                    }
//                );
//                categoryName = "Data Add-ons";
//                categoryType = 7;
//            }
//        );
//        primaryOfferName = MamenPrepaid;
//        subscriberStatus = Active;
//    };
//    success = 1;
//}

struct MamenAddons: Mappable {
    
    var billingFormat: Int!
    var bundleGroups: [MamenBundleGroup]!
    var primaryOfferName: String!
    var subscriberStatus: String!
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        billingFormat <- map["data.billingFormat"]
        bundleGroups <- map["data.bundleGroup"]
        primaryOfferName <- map["data.primaryOfferName"]
        subscriberStatus <- map["data.subscriberStatus"]
    }
}

struct MamenBundle: Mappable {
    
    var bundleId: Int!
    var bundleName: String!
    var bundlePrice: Double!
    var bundleSystemName: String!
    var bundleType: Int!
    var expiresIn: Int!
    var fundAvailable: Bool!
    var grantAmount: Double!
    var isRecurring: Bool!
    var purchasePrice: Double!
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        bundleId <- map["bundleId"]
        bundleName <- map["bundleName"]
        bundlePrice <- map["bundlePrice"]
        bundleSystemName <- map["bundleSystemName"]
        bundleType <- map["bundleType"]
        expiresIn <- map["expiresIn"]
        fundAvailable <- map["fundAvailable"]
        grantAmount <- map["grantAmount"]
        isRecurring <- map["isRecurring"]
        purchasePrice <- map["purchasePrice"]
    }
}

struct MamenBundleGroup: Mappable {
    
    var bundles: [MamenBundle]!
    var categoryName: String!
    var categoryType: Int!
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        bundles <- map["bundles"]
        categoryName <- map["categoryName"]
        categoryType <- map["categoryType"]
    }
}
