//
//  Enums.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 7/11/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation

/**
     Mamen transmission type.
     - voice: For voice usage.
     - data: For data usage.
     - text: For text usage.
 */
enum Transmission: String {
    
    case voice = "voice"
    case data = "data"
    case text = "text"
    
    /**
         
     */
    func lowercasedString() -> String {
        return self.rawValue.lowercased()
    }
    
    func uppercasedString() -> String {
        return self.rawValue.uppercased()
    }
}

enum WalletType {
    case build
    case takeback
}

