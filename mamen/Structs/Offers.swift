//
//  Offers.swift
//  mamen
//
//  Created by Ushan Hattotuwa on 8/17/17.
//  Copyright © 2017 Ushan Hattotuwa. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

struct Merchant: Mappable {
    
    var id: Int!
    var name: String!
    var logoUrl: String?
    var backgroundUrl: String?
    var description: String!
    var offersCount: Int!
    var offers = [Offer]()
    var isFavorite: Bool!
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        logoUrl <- map["image"]
        backgroundUrl <- map["thumbnail"]
        description <- map["description"]
        offersCount <- map["offersCount"]
        offers <- map["offers"]
        isFavorite <- map["is_favourite"]
    }
}

struct Offer: Mappable {
    
    var id: Int!
    var merchantId: Int!
    var title: String!
    var offerDescription: String!
    var code: String!
    var imageUrl: String!
    var discount: Double!
    var startDateString: String!
    var endDateString: String!
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        merchantId <- map["merchant_id"]
        title <- map["title"]
        offerDescription <- map["description"]
        code <- map["code"]
        imageUrl <- map["image"]
        discount <- map["discount"]
        startDateString <- map["start_date"]
        endDateString <- map["end_date"]
    }
    
    
}
